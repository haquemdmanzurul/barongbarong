$(function(){
	$('a').click(function(e){
		var toggle = $(this).attr('data-toggle');
		if(toggle) {
			e.preventDefault();
		}
		else if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
		        $('html,body').animate({
		          scrollTop: target.offset().top-200
		        }, 500);
		        e.preventDefault();
	      	} 
	    } 
	});

	$('.main-toggle').click(function(){
		$('.main-menu').toggleClass('show');
	});

	$(window).scroll(function(){
        $('.full').each(function(r){
            var pos = $(this).offset().top+200;
            var scrolled = $(window).scrollTop();
            $('.full').css('top', -(scrolled * 0.5) + 'px');         
        });
    });

});

$(window).load(function(){
	var $container = $('#isotope');

	$container.isotope({
	  itemSelector: '.item'
	});
});
