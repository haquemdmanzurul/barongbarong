jQuery(document).ready(function($) {

	// Close export metabox by default
	$("#cpt_code_metabox.postbox").addClass("closed");

	/*
	|--------------------------------------------------------------------------
	| Show / Hide Defaults step status
	|--------------------------------------------------------------------------
	*/
	$("#rc_wctg_general").show();
	$("#rc_wctg_features").hide();
	$("#rc_wctg_labels").hide();
	$("#rc_wctg_visibility").hide();
	$("#rc_wctg_options").hide();
	$("#rc_wctg_capabilities").hide();

	/*
	|--------------------------------------------------------------------------
	| General: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Next
	$("#rc_wctg_next_button_general").click(function(){
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_general").hide();
		$("#rc_wctg_features").show();
		$("#rc_wctg_next_button_features").show();
		
		if($('#rc_wctg_mode_basic').is(':checked')){
			$("#rc_wctg_next_button_features").hide();
		}
		
	});
	// Step Link
	$("#rc_wctg_tab_general").click(function(){
		// Add step class
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Remove step class
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_general").show();
		
		// hide other tabs
		$("#rc_wctg_features").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
	});
	
	/*
	|--------------------------------------------------------------------------
	| Features: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_features").click(function(){
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_features").hide();
		$("#rc_wctg_general").show();
	});
	// Next
	$("#rc_wctg_next_button_features").click(function(){
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_features").hide();
		$("#rc_wctg_labels").show();
	});
	// Step Link
	$("#rc_wctg_tab_features").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		$("#rc_wctg_next_button_features").show();
		
		// Remove step class
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_features").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
		
		if($('#rc_wctg_mode_basic').is(':checked')){
			$("#rc_wctg_next_button_features").hide();
		}
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| Labels: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_labels").click(function(){
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_features").show();
	});
	// Next
	$("#rc_wctg_next_button_labels").click(function(){
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").show();
	});
	// Step Link
	$("#rc_wctg_tab_labels").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Remove step class
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_labels").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_features").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| Visibility: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_visibility").click(function(){
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_labels").show();
	});
	// Next
	$("#rc_wctg_next_button_visibility").click(function(){
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").show();
	});
	// Step Link
	$("#rc_wctg_tab_visibility").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Remove step class
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_visibility").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_features").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| Options: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_options").click(function(){
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_options").hide();
		$("#rc_wctg_visibility").show();
	});
	// Next
	$("#rc_wctg_next_button_options").click(function(){
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").show();
	});
	// Step Link
	$("#rc_wctg_tab_options").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Remove step class
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_options").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_features").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_capabilities").hide();
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| capabilities: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_capabilities").click(function(){
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_capabilities").hide();
		$("#rc_wctg_options").show();
	});
	// Step Link
	$("#rc_wctg_tab_capabilities").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_features").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Show current tab
		$("#rc_wctg_capabilities").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_features").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| Switch Expert / Basic mode
	|--------------------------------------------------------------------------
	*/
	// Basic
	$("#rc_wctg_mode_basic").click(function(){
		$("#rc_wctg_general").show();
		$("#rc_wctg_features").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
		$("#rc_wctg_next_button_features").hide();
		
		$("#rc_wctg_tab_labels").hide();
		$("#rc_wctg_tab_visibility").hide();
		$("#rc_wctg_tab_options").hide();
		$("#rc_wctg_tab_capabilities").hide();
	});
	
	$("#rc_wctg_mode_expert").click(function(){
		$("#rc_wctg_general").show();
		$("#rc_wctg_features").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
		$("#rc_wctg_tab_labels").show();
		$("#rc_wctg_tab_visibility").show();
		$("#rc_wctg_tab_options").show();
		$("#rc_wctg_tab_capabilities").show();
	});
	
	/*
	|--------------------------------------------------------------------------
	| CHECK EMPTY REQUIRED FIELDS
	|--------------------------------------------------------------------------
	*/
	// CPT name
	$('#rc_wctg_cpt_name').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }
	});
	// Singular label
	$('#rc_wctg_singular_name').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }
	});
	// Plural label
	$('#rc_wctg_plural_name').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }
	});
	
	/*
	|--------------------------------------------------------------------------
	| AUTO FILL LABELS FIELDS
	|--------------------------------------------------------------------------
	*/
	$('#rc_wctg_singular_name').blur(function()
	{
	    $('#rc_wctg_add_new').val( 'Add new');
	    $('#rc_wctg_add_new_item').val( 'Add new ' + $('#rc_wctg_singular_name').val());
	    $('#rc_wctg_new_item').val( 'New ' + $('#rc_wctg_singular_name').val());
	    $('#rc_wctg_edit_item').val( 'Edit ' + $('#rc_wctg_singular_name').val());
	    $('#rc_wctg_view_item').val( 'View ' + $('#rc_wctg_singular_name').val());
	    $('#rc_wctg_not_found').val( 'No ' + $('#rc_wctg_singular_name').val() + ' found');
	    $('#rc_wctg_not_found_in_trash').val( 'No ' + $('#rc_wctg_singular_name').val() + ' found in trash');
	    $('#rc_wctg_parent_item_colon').val( 'Parent ' + $('#rc_wctg_singular_name').val());
	});
	$('#rc_wctg_plural_name').blur(function()
	{
	    $('#rc_wctg_all_items').val( 'All ' + $('#rc_wctg_plural_name').val());
	    $('#rc_wctg_search_items').val( 'Search ' + $('#rc_wctg_plural_name').val());
	    $('#rc_wctg_menu_text').val( $('#rc_wctg_plural_name').val());
	});
	
	/*
	|--------------------------------------------------------------------------
	| AUTO FILL CAPABILITIES FIELDS
	|--------------------------------------------------------------------------
	*/
	$("#rc_wctg_advanced_capabilities").click(function()
	{
		// If checked
		if ($("#rc_wctg_advanced_capabilities").is(":checked"))
		{
			$('#rc_wctg_capabilities_edit_post').val('edit_post');
			$('#rc_wctg_capabilities_edit_posts').val('edit_posts');
			$('#rc_wctg_capabilities_edit_others_posts').val('edit_others_posts');
			$('#rc_wctg_capabilities_publish_posts').val('publish_posts');
			$('#rc_wctg_capabilities_read_post').val('read_post');
			$('#rc_wctg_capabilities_read_private_posts').val('read_private_posts');
			$('#rc_wctg_capabilities_delete_post').val('delete_post');
		}
		else
		{
			$('#rc_wctg_capabilities_edit_post').val('');
			$('#rc_wctg_capabilities_edit_posts').val('');
			$('#rc_wctg_capabilities_edit_others_posts').val('');
			$('#rc_wctg_capabilities_publish_posts').val('');
			$('#rc_wctg_capabilities_read_post').val('');
			$('#rc_wctg_capabilities_read_private_posts').val('');
			$('#rc_wctg_capabilities_delete_post').val('');
		}
	});
	
	/*
	|--------------------------------------------------------------------------
	| MEDIA UPLOADER
	|--------------------------------------------------------------------------
	*/
	// Uploading files
	var file_frame;
	var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
	var set_to_post_id = 10; // Set this
 
  jQuery('.rc_wctg_upload_image_button').live('click', function( event ){
  
    event.preventDefault();
    
	var button = $(this);
 
    // If the media frame already exists, reopen it.
    if ( file_frame ) {
      // Set the post ID to what we want
      file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
      // Open frame
      file_frame.open();
      return;
    } else {
      // Set the wp.media post id so the uploader grabs the ID we want when initialised
      wp.media.model.settings.post.id = set_to_post_id;
    }
 
    // Create the media frame.
    file_frame = wp.media.frames.file_frame = wp.media({
      title: jQuery( this ).data( 'uploader_title' ),
      button: {
        text: jQuery( this ).data( 'uploader_button_text' ),
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });
 
    // When an image is selected, run a callback.
    file_frame.on( 'select', function() {
      // We set multiple to false so only get one image from the uploader
      attachment = file_frame.state().get('selection').first().toJSON();
 
      // Do something with attachment.id and/or attachment.url here
      $(button).prev('input').val(attachment.url);

      // Restore the main post ID
      wp.media.model.settings.post.id = wp_media_post_id;
    });
 
    // Finally, open the modal
    file_frame.open();
  });
  
  // Restore the main ID when the add media button is pressed
  jQuery('a.add_media').on('click', function() {
    wp.media.model.settings.post.id = wp_media_post_id;
  });
	

});