jQuery(document).ready(function($) {

	// Close export metabox by default
	$("#cpt_code_metabox.postbox").addClass("closed");

	
	/*
	|--------------------------------------------------------------------------
	| Show / Hide Defaults step status
	|--------------------------------------------------------------------------
	*/
	$("#rc_wctg_general").show();
	$("#rc_wctg_labels").hide();
	$("#rc_wctg_visibility").hide();
	$("#rc_wctg_options").hide();
	$("#rc_wctg_capabilities").hide();
	
	/*
	|--------------------------------------------------------------------------
	| General: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Next
	$("#rc_wctg_next_button_general").click(function(){
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_general").hide();
		$("#rc_wctg_labels").show();
		$("#rc_wctg_next_button_labels").show();
		
		if($('#rc_wctg_mode_basic').is(':checked')){
			$("#rc_wctg_next_button_labels").hide();
		}
		
	});
	// Step Link
	$("#rc_wctg_tab_general").click(function(){
		// Add step class
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Remove step class
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_permalinks").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_general").show();
		
		// hide other tabs
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_permalinks").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
	});
	
	/*
	|--------------------------------------------------------------------------
	| Labels: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_labels").click(function(){
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_general").show();
	});
	// Next
	$("#rc_wctg_next_button_labels").click(function(){
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").show();
	});
	// Step Link
	$("#rc_wctg_tab_labels").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Remove step class
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_permalinks").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_labels").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_permalinks").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| Visibility: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_visibility").click(function(){
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_labels").show();
	});
	// Next
	$("#rc_wctg_next_button_visibility").click(function(){
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").show();
	});
	// Step Link
	$("#rc_wctg_tab_visibility").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Remove step class
		$("#rc_wctg_tab_permalinks").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_visibility").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
	});
		
	/*
	|--------------------------------------------------------------------------
	| Options: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_options").click(function(){
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_options").hide();
		$("#rc_wctg_visibility").show();
	});
	// Next
	$("#rc_wctg_next_button_options").click(function(){
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").show();
	});
	// Step Link
	$("#rc_wctg_tab_options").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Remove step class
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		
		// Show current tab
		$("#rc_wctg_options").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_capabilities").hide();
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| capabilities: previous / next buttons
	|--------------------------------------------------------------------------
	*/
	// Previous
	$("#rc_wctg_previous_button_capabilities").click(function(){
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-done").addClass("rc_wctg_step_tracker-todo");
		$("#rc_wctg_capabilities").hide();
		$("#rc_wctg_options").show();
	});
	// Step Link
	$("#rc_wctg_tab_capabilities").click(function(){
		// Add step class
		$("#rc_wctg_tab_general").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_permalinks").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_labels").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_visibility").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_options").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		$("#rc_wctg_tab_capabilities").removeClass("rc_wctg_step_tracker-todo").addClass("rc_wctg_step_tracker-done");
		
		// Show current tab
		$("#rc_wctg_capabilities").show();
		
		// Hide other tabs
		$("#rc_wctg_general").hide();
		$("#rc_wctg_permalinks").hide();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_options").hide();
		
	});
	
	/*
	|--------------------------------------------------------------------------
	| Switch Expert / Basic mode
	|--------------------------------------------------------------------------
	*/
	// Basic
	$("#rc_wctg_mode_basic").click(function(){
		$("#rc_wctg_general").show();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_permalinks").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
		$("#rc_wctg_next_button_labels").hide();
		
		$("#rc_wctg_tab_permalinks").hide();
		$("#rc_wctg_tab_visibility").hide();
		$("#rc_wctg_tab_options").hide();
		$("#rc_wctg_tab_capabilities").hide();
	});
	
	$("#rc_wctg_mode_expert").click(function(){
		$("#rc_wctg_general").show();
		$("#rc_wctg_labels").hide();
		$("#rc_wctg_visibility").hide();
		$("#rc_wctg_permalinks").hide();
		$("#rc_wctg_options").hide();
		$("#rc_wctg_capabilities").hide();
		
		$("#rc_wctg_tab_labels").show();
		$("#rc_wctg_tab_permalinks").show();
		$("#rc_wctg_tab_visibility").show();
		$("#rc_wctg_tab_options").show();
		$("#rc_wctg_tab_capabilities").show();
	});
	
	/*
	|--------------------------------------------------------------------------
	| CHECK EMPTY REQUIRED FIELDS
	|--------------------------------------------------------------------------
	*/
	// CPT name
	$('#rc_wctg_taxonomy_name').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }
	});
	// Singular label
	$('#rc_wctg_singular_name').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }
	});
	// Plural label
	$('#rc_wctg_plural_name').blur(function()
	{
	    if(!$.trim(this.value).length) {
	          $(this).addClass('required-field');
	    } else {
		    $(this).removeClass('required-field');
	    }
	});
	
	/*
	|--------------------------------------------------------------------------
	| AUTO FILL LABELS FIELDS
	|--------------------------------------------------------------------------
	*/
	$('#rc_wctg_singular_name').blur(function()
	{
	    $('#rc_wctg_edit_item').val( 'Edit ' + $('#rc_wctg_singular_name').val() );
	    $('#rc_wctg_view_item').val( 'View ' + $('#rc_wctg_singular_name').val() );
	    $('#rc_wctg_update_item').val( 'Update ' + $('#rc_wctg_singular_name').val() );
	    $('#rc_wctg_add_new_item').val( 'Add New ' + $('#rc_wctg_singular_name').val() );
	    $('#rc_wctg_new_item_name').val( 'New ' + $('#rc_wctg_singular_name').val() + ' Name' );
	    $('#rc_wctg_parent_item').val( 'Parent ' + $('#rc_wctg_singular_name').val() );
	    $('#rc_wctg_parent_item_colon').val( 'Parent: ' + $('#rc_wctg_singular_name').val() );
	});
	$('#rc_wctg_plural_name').blur(function()
	{
	    $('#rc_wctg_menu_text').val( $('#rc_wctg_plural_name').val() );
	    $('#rc_wctg_all_items').val( 'All ' + $('#rc_wctg_plural_name').val() );
	    $('#rc_wctg_search_items').val( 'Search ' + $('#rc_wctg_plural_name').val() );
	    $('#rc_wctg_popular_items').val( 'Popular ' + $('#rc_wctg_plural_name').val() );
	    $('#rc_wctg_separate_items').val( 'Separate ' + $('#rc_wctg_plural_name').val() + ' with commas' );
	    $('#rc_wctg_add_remove_items').val( 'Add or remove ' + $('#rc_wctg_plural_name').val() );
	    $('#rc_wctg_choose_items').val( 'Choose from the most used ' + $('#rc_wctg_plural_name').val() );
	    $('#rc_wctg_not_found').val( 'No ' + $('#rc_wctg_plural_name').val() + ' found' );
	});
	
	/*
	|--------------------------------------------------------------------------
	| AUTO FILL CAPABILITIES FIELDS
	|--------------------------------------------------------------------------
	*/
	$("#rc_wctg_advanced_capabilities").click(function()
	{
		// If checked
		if ($("#rc_wctg_advanced_capabilities").is(":checked"))
		{
			$('#rc_wctg_capabilities_edit_terms').val('manage_categories');
			$('#rc_wctg_capabilities_manage_terms').val('manage_categories');
			$('#rc_wctg_capabilities_delete_terms').val('manage_categories');
			$('#rc_wctg_capabilities_assign_terms').val('edit_posts');
		}
		else
		{
			$('#rc_wctg_capabilities_edit_terms').val('');
			$('#rc_wctg_capabilities_manage_terms').val('');
			$('#rc_wctg_capabilities_delete_terms').val('');
			$('#rc_wctg_capabilities_assign_terms').val('');
		}
	});
	
	

});