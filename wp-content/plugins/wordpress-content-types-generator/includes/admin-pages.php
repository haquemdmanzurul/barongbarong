<?php

/**
 * Add Options Link
 *
 * Creates the admin submenu pages.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_add_options_link() {
	
	global $rc_wctg_settings_page;
	global $rc_wctg_options;
	
	if( isset($rc_wctg_options['minimum_level']) ) {
		$level = $rc_wctg_options['minimum_level'];
	} else {
		$level = 'administrator';
	}
	
	// Main Menu Page
	add_menu_page( __('WordPress Post Types Manager', 'rc_wctg'), __('CPTs Manager', 'rc_wctg'), $level,  'wordpress-post-types-manager', 'rc_wctg_home');
	
	// Settings Page
	$rc_wctg_settings_page = add_submenu_page( 'wordpress-post-types-manager', __('Settings', 'rc_wctg'), __('Settings', 'rc_wctg'), 'manage_options', 'rc_wctg-settings', 'rc_wctg_options_page' );

	
}
add_action( 'admin_menu', 'rc_wctg_add_options_link', 10 );