<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
   	
/**
 * Adds Post Thumbnail support
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function rc_wctg_add_thumbnail_support() {
	if(!current_theme_supports('post-thumbnails')) {
		add_theme_support('post-thumbnails');
	}
}
add_action('after_setup_theme', 'rc_wctg_add_thumbnail_support');