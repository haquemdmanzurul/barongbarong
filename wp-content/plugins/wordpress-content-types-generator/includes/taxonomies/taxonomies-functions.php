<?php

/**
 * Register custom post types
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function rc_wctg_init_custom_taxonomies() {

	// Create the Query
	$posts_per_page = -1;
	$post_type 		= 'taxonomy';
				
	$query = new WP_Query( array ( 
								'post_type' => $post_type,
								'posts_per_page' => $posts_per_page,
								) 
						);
	
	//Get post type count
	$post_count = $query->post_count;
	$i = 1;
	
		// Loop
		while ($query->have_posts()) : $query->the_post();
		
        global $post;
        
        /*
        |--------------------------------------------------------------------------
        | General
        |--------------------------------------------------------------------------
        */
	    $rc_wctg_taxonomy_name      = get_post_meta($post->ID, 'rc_wctg_taxonomy_name', true);
	    $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
	    $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
	    $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
	    $rc_wctg_link_to       		= get_post_meta($post->ID, 'rc_wctg_link_to', true);

        $rc_wctg_hierarchical = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        if ($rc_wctg_hierarchical == "on") {
            $rc_wctg_hierarchical = 'true';
        } else {
            $rc_wctg_hierarchical = 'false';
        }
        
        /*
        |--------------------------------------------------------------------------
        | LABELS
        |--------------------------------------------------------------------------
        */
        $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);
        $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
        $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
        $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
        $rc_wctg_update_item        = get_post_meta($post->ID, 'rc_wctg_update_item', true);
        $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
        $rc_wctg_new_item_name      = get_post_meta($post->ID, 'rc_wctg_new_item_name', true);
        $rc_wctg_parent_item  		= get_post_meta($post->ID, 'rc_wctg_parent_item', true);
        $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
        $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
        $rc_wctg_popular_items   	= get_post_meta($post->ID, 'rc_wctg_popular_items', true);
        $rc_wctg_separate_items   	= get_post_meta($post->ID, 'rc_wctg_separate_items', true);
        $rc_wctg_add_remove_items   = get_post_meta($post->ID, 'rc_wctg_add_remove_items', true);
        $rc_wctg_choose_items   	= get_post_meta($post->ID, 'rc_wctg_choose_items', true);
        $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
        
        /*
        |--------------------------------------------------------------------------
        | VISIBILITY
        |--------------------------------------------------------------------------
        */
        $rc_wctg_public = get_post_meta($post->ID, 'rc_wctg_public', true);
        if ($rc_wctg_public == "on") {
            $rc_wctg_public = 'true';
        } else {
            $rc_wctg_public = 'false';
        }
        $rc_wctg_show_ui = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
        if ($rc_wctg_show_ui == "on") {
            $rc_wctg_show_ui = 'true';
        } else {
            $rc_wctg_show_ui = 'false';
        }
        $rc_wctg_show_tagcloud = get_post_meta($post->ID, 'rc_wctg_show_tagcloud', true);
        if ($rc_wctg_show_tagcloud == "on") {
            $rc_wctg_show_tagcloud = 'true';
        } else {
            $rc_wctg_show_tagcloud = 'false';
        }
        $rc_wctg_show_admin_column = get_post_meta($post->ID, 'rc_wctg_show_admin_column', true);
        if ($rc_wctg_show_admin_column == "on") {
            $rc_wctg_show_admin_column = 'true';
        } else {
            $rc_wctg_show_admin_column = 'false';
        }
        $rc_wctg_show_in_nav_menus = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true);
        if ($rc_wctg_show_in_nav_menus == "on") {
            $rc_wctg_show_in_nav_menus = 'true';
        } else {
            $rc_wctg_show_in_nav_menus = 'false';
        }
        
        /*
        |--------------------------------------------------------------------------
        | OPTIONS
        |--------------------------------------------------------------------------
        */
        $rc_wctg_query_var = get_post_meta($post->ID, 'rc_wctg_query_var', true);
        if ($rc_wctg_query_var == "on") {
            $rc_wctg_query_var = 'true';
        } else {
            $rc_wctg_query_var = 'false';
        }
        $rc_wctg_rewrite = get_post_meta($post->ID, 'rc_wctg_rewrite', true);
        if ($rc_wctg_rewrite == "on") {
            $rc_wctg_rewrite = 'true';
        } else {
            $rc_wctg_rewrite = 'false';
        }
        
        /*
        |--------------------------------------------------------------------------
        | Capabilities
        |--------------------------------------------------------------------------
        */
	    $rc_wctg_advanced_capabilities     = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
		$rc_wctg_capabilities_edit_terms   = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_terms', true);
		$rc_wctg_capabilities_manage_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_manage_terms', true);
		$rc_wctg_capabilities_delete_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_terms', true);
		$rc_wctg_capabilities_assign_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_assign_terms', true);

        if ($rc_wctg_advanced_capabilities == "on") {
        
        	$rc_wctg_capabilities = array(
							            'edit_terms'   => $rc_wctg_capabilities_edit_terms,
							            'manage_terms' => $rc_wctg_capabilities_manage_terms,
							            'delete_terms' => $rc_wctg_capabilities_delete_terms,
							            'assign_terms' => $rc_wctg_capabilities_assign_terms
        							);
        } else {
	        $rc_wctg_capabilities = array();
        }

        
        /*
        |--------------------------------------------------------------------------
        | Register Taxonomy
        |--------------------------------------------------------------------------
        */
        
		// Create Taxonomy Labels      
		$labels = array(
			'name'                       => _x( $rc_wctg_taxonomy_name, 'taxonomy general name' ),
			'singular_name'              => _x( $rc_wctg_singular_name, 'taxonomy singular name' ),
			'search_items'               => __( $rc_wctg_search_items ),
			'popular_items'              => __( $rc_wctg_popular_items ),
			'all_items'                  => __( $rc_wctg_all_items ),
			'parent_item'                => __( $rc_wctg_parent_item ),
			'parent_item_colon'          => __( $rc_wctg_parent_item_colon ),
			'edit_item'                  => __( $rc_wctg_edit_item ),
			'view_item'                  => __( $rc_wctg_view_item ),
			'update_item'                => __( $rc_wctg_update_item ),
			'add_new_item'               => __( $rc_wctg_add_new_item ),
			'new_item_name'              => __( $rc_wctg_new_item_name ),
			'add_or_remove_items'        => __( $rc_wctg_add_remove_items ),
			'choose_from_most_used'      => __( $rc_wctg_choose_items ),
			'separate_items_with_commas' => __( $rc_wctg_separate_items ),
			'menu_name'                  => __( $rc_wctg_menu_text )
		);


		// Taxonomy Capabilities  
		$capabilities = array(
		    'edit_terms'   => $rc_wctg_capabilities_edit_terms,
		    'manage_terms' => $rc_wctg_capabilities_manage_terms,
		    'delete_terms' => $rc_wctg_capabilities_delete_terms,
		    'assign_terms' => $rc_wctg_capabilities_assign_terms
		);

		// Taxonomy Arguments  
		$args = array(
		    'labels'            => $labels,
		    'hierarchical'      => $rc_wctg_hierarchical,
		    'description'       => $rc_wctg_description,
		    'public'            => $rc_wctg_public,
		    'show_ui'           => $rc_wctg_show_ui,
		    'show_tagcloud'     => $rc_wctg_show_tagcloud,
		    'show_in_nav_menus' => $rc_wctg_show_in_nav_menus,
		    'show_admin_column' => $rc_wctg_show_admin_column,
		    'query_var'         => $rc_wctg_query_var,
		    'rewrite'           => $rc_wctg_rewrite,
	    );
	    
		// Advanced capabilities
		if( isset($rc_wctg_advanced_capabilities) && $rc_wctg_advanced_capabilities == 'on') {
			$add_capabilities = array('capabilities' => $capabilities);
			array_push($args, $add_capabilities);
		}

        register_taxonomy( $rc_wctg_taxonomy_name, $rc_wctg_link_to, $args );

    endwhile;
}

// Load Custom taxonomies generated by the plugin
add_action('init', 'rc_wctg_init_custom_taxonomies');