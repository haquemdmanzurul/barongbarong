<?php

/*
|--------------------------------------------------------------------------
| taxonomies Columns
|--------------------------------------------------------------------------
*/

/**
 * Load custom taxonomy columns
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_taxonomy_columns($columns)
{

	$newcolumns = array(
		"cb"       	  => "<input type  = \"checkbox\" />",
		"title"       => "",
		"labels"      => esc_html__('Labels', 'rc_wctg'),
		"CPTs"        => esc_html__('CPTs', 'rc_wctg'),
		"description" => esc_html__('Description', 'rc_wctg'),
	);

	
	$columns = array_merge($newcolumns, $columns);
	
	return $columns;
}


/**
 * Charge taxonomies columns content
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_taxonomy_columns_content($column)
{
	global $post, $typenow;
	
	$cpt_id = $post->ID;
	
	// CPT data
	$rc_wctg_taxonomy_name      = get_post_meta($post->ID, 'rc_wctg_cpt_name', true);
    $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
    $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
    $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
    $rc_wctg_link_to        	= get_post_meta($post->ID, 'rc_wctg_link_to', true);

	switch ($column)
	{
		case "CPTs":
       	 	$count_cpt = count($rc_wctg_link_to);
       	 	$j = 1;
		    foreach( $rc_wctg_link_to as $rc_wctg_cpt ) {

		    	$query = new WP_Query( array( 'meta_key' => 'rc_wctg_cpt_name', 'meta_value' => $rc_wctg_cpt, 'meta_compare' => '=', 'post_type' => 'cpt' ) );
				
				$post_count = $query->post_count;

				if( $post_count >= 1) :			
					while ($query->have_posts()) : $query->the_post();
					        $cpt_id = get_the_ID() ;
					        echo '<a href="'.admin_url().'post.php?post='.$cpt_id.'&action=edit">'.$rc_wctg_cpt.'</a>';
					endwhile;
				else :
					_e( 'No CPT assigned.' );
				endif;
				
				wp_reset_query();

		        if( $j < $count_cpt )
		        	echo ', ';
		        $j++;
		    }
		break;	
		case "labels":
			echo __('Singular', 'rc_wctg').': '.$rc_wctg_singular_name.'<br />'.__('Plural', 'rc_wctg').': '.$rc_wctg_plural_name.'';
		break;	
		case "description":
			echo $rc_wctg_description;
		break;	
		
	}
}

/**
 * Load taxonomies columns
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_load_taxonomy_columns() {
	
	global $typenow;
	
	if( $typenow == 'taxonomy' ) {
		add_filter("manage_edit-taxonomy_columns", "rc_wctg_taxonomy_columns");
		add_action("manage_posts_custom_column",  "rc_wctg_taxonomy_columns_content");
	}
}

add_action("admin_init",  "rc_wctg_load_taxonomy_columns");