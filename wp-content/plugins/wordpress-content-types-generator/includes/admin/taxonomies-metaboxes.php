<?php

function rc_wctg_taxonomy_save_postdata() {

    global $post;
    
    if ( isset($_POST['rc_wctg_submit']) && $_POST['rc_wctg_submit'] == 'true') {
    
    	/*
		|--------------------------------------------------------------------------
		| GET META FIELDS VALUES
		|--------------------------------------------------------------------------
		*/
    	
    	// Mode
    	$rc_wctg_mode       		= get_post_meta($post->ID, 'rc_wctg_mode', true);
    	        
    	// General
        $rc_wctg_taxonomy_name      = get_post_meta($post->ID, 'rc_wctg_taxonomy_name', true);
        $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
        $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
        $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
        $rc_wctg_hierarchical       = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        $rc_wctg_link_to	        = get_post_meta($post->ID, 'rc_wctg_link_to', true);

        // Labels
        $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);
        $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
        $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
        $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
        $rc_wctg_update_item        = get_post_meta($post->ID, 'rc_wctg_update_item', true);
        $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
        $rc_wctg_new_item_name      = get_post_meta($post->ID, 'rc_wctg_new_item_name', true);
        $rc_wctg_parent_item  		= get_post_meta($post->ID, 'rc_wctg_parent_item', true);
        $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
        $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
        $rc_wctg_popular_items   	= get_post_meta($post->ID, 'rc_wctg_popular_items', true);
        $rc_wctg_separate_items   	= get_post_meta($post->ID, 'rc_wctg_separate_items', true);
        $rc_wctg_add_remove_items   = get_post_meta($post->ID, 'rc_wctg_add_remove_items', true);
        $rc_wctg_choose_items   	= get_post_meta($post->ID, 'rc_wctg_choose_items', true);
        $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
        
        // Visibility
        $rc_wctg_public             = get_post_meta($post->ID, 'rc_wctg_public', true);
        $rc_wctg_show_ui            = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
        $rc_wctg_show_tagcloud      = get_post_meta($post->ID, 'rc_wctg_show_tagcloud', true); 
        $rc_wctg_show_admin_column  = get_post_meta($post->ID, 'rc_wctg_show_admin_column', true); 
        $rc_wctg_show_in_nav_menus  = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true); 
        
        // Options
        $rc_wctg_query_var          = get_post_meta($post->ID, 'rc_wctg_query_var', true); 
        $rc_wctg_rewrite            = get_post_meta($post->ID, 'rc_wctg_rewrite', true); 
        
        // Capabilities
        $rc_wctg_advanced_capabilities     = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
        $rc_wctg_capabilities_edit_terms   = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_terms', true);
        $rc_wctg_capabilities_manage_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_manage_terms', true);
        $rc_wctg_capabilities_delete_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_terms', true);
        $rc_wctg_capabilities_assign_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_assign_terms', true);

		/*
		|--------------------------------------------------------------------------
		| UPDATE META FIELDS VALUES
		|--------------------------------------------------------------------------
		*/
        
        // Mode
        if( isset( $_POST['rc_wctg_mode'] ) ) { update_post_meta($post->ID, 'rc_wctg_mode', sanitize_title($_POST['rc_wctg_mode']), $rc_wctg_mode); }
        
        // Update General fields
        $rc_wctg_update_general_fields = array(
        							'rc_wctg_taxonomy_name', 
        							'rc_wctg_singular_name',
        							'rc_wctg_plural_name', 
        							'rc_wctg_description', 
        							'rc_wctg_hierarchical', 
        							'rc_wctg_link_to'
        							);

        foreach( $rc_wctg_update_general_fields as $rc_wctg_update_general_field) {
        	if( isset( $_POST[$rc_wctg_update_general_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_general_field, $_POST[$rc_wctg_update_general_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_general_field); 
        	} // end if
        } // end foreach
        
        // Update Labels
        $rc_wctg_update_labels_fields = array(
        							'rc_wctg_menu_text', 
        							'rc_wctg_all_items',
        							'rc_wctg_edit_item', 
        							'rc_wctg_view_item', 
        							'rc_wctg_update_item', 
        							'rc_wctg_add_new_item', 
        							'rc_wctg_new_item_name', 
        							'rc_wctg_parent_item', 
        							'rc_wctg_parent_item_colon', 
        							'rc_wctg_search_items', 
        							'rc_wctg_popular_items', 
        							'rc_wctg_separate_items', 
        							'rc_wctg_add_remove_items', 
        							'rc_wctg_choose_items', 
        							'rc_wctg_not_found'
        							);

        foreach( $rc_wctg_update_labels_fields as $rc_wctg_update_labels_field) {
        	if( isset( $_POST[$rc_wctg_update_labels_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_labels_field, $_POST[$rc_wctg_update_labels_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_labels_field); 
        	} // end if
        } // end foreach

        // Update Visibility
        $rc_wctg_update_visibility_fields = array(
        							'rc_wctg_public', 
        							'rc_wctg_show_ui',
        							'rc_wctg_show_tagcloud', 
        							'rc_wctg_show_admin_column', 
        							'rc_wctg_show_in_menu_val'
        							);

        foreach( $rc_wctg_update_visibility_fields as $rc_wctg_update_visibility_field) {
        	if( isset( $_POST[$rc_wctg_update_visibility_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_visibility_field, $_POST[$rc_wctg_update_visibility_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_visibility_field); 
        	} // end if
        } // end foreach
        
        // Update Options
        $rc_wctg_update_options_fields = array(
        							'rc_wctg_query_var', 
        							'rc_wctg_rewrite'
        							);

        foreach( $rc_wctg_update_options_fields as $rc_wctg_update_options_field) {
        	if( isset( $_POST[$rc_wctg_update_options_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_options_field, $_POST[$rc_wctg_update_options_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_options_field); 
        	} // end if
        } // end foreach

        // Update Capabilities
        $rc_wctg_update_capabilities_fields = array(
        							'rc_wctg_advanced_capabilities', 
        							'rc_wctg_capabilities_edit_terms',
        							'rc_wctg_capabilities_manage_terms',
        							'rc_wctg_capabilities_delete_terms',
        							'rc_wctg_capabilities_assign_terms'
        							);

        foreach( $rc_wctg_update_capabilities_fields as $rc_wctg_update_capabilities_field) {
        	if( isset( $_POST[$rc_wctg_update_capabilities_field] ) ) { 
        		update_post_meta( $post->ID, $rc_wctg_update_capabilities_field, $_POST[$rc_wctg_update_capabilities_field] ); 
        	} else { 
        		delete_post_meta($post->ID, $rc_wctg_update_capabilities_field); 
        	} // end if
        } // end foreach

        
        /*
        |--------------------------------------------------------------------------
        | BASIC MODE (forces values)
        |--------------------------------------------------------------------------
        */        
        if( isset( $_POST['rc_wctg_mode'] ) && $_POST['rc_wctg_mode'] == 'basic') {
	        update_post_meta($post->ID, 'rc_wctg_public', 'on' );
	        update_post_meta($post->ID, 'rc_wctg_show_ui', 'on' );
        }
        
    }
}

function rc_wctg_taxonomy_metabox() {
    global $post;
    
	// Mode
	$rc_wctg_mode       		= get_post_meta($post->ID, 'rc_wctg_mode', true);
	        
	// General
    $rc_wctg_taxonomy_name      = get_post_meta($post->ID, 'rc_wctg_taxonomy_name', true);
    $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
    $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
    $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
    $rc_wctg_hierarchical       = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
    $rc_wctg_link_to       		= get_post_meta($post->ID, 'rc_wctg_link_to', true);

    // Labels
    $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);
    $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
    $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
    $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
    $rc_wctg_update_item        = get_post_meta($post->ID, 'rc_wctg_update_item', true);
    $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
    $rc_wctg_new_item_name      = get_post_meta($post->ID, 'rc_wctg_new_item_name', true);
    $rc_wctg_parent_item  		= get_post_meta($post->ID, 'rc_wctg_parent_item', true);
    $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
    $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
    $rc_wctg_popular_items   	= get_post_meta($post->ID, 'rc_wctg_popular_items', true);
    $rc_wctg_separate_items   	= get_post_meta($post->ID, 'rc_wctg_separate_items', true);
    $rc_wctg_add_remove_items   = get_post_meta($post->ID, 'rc_wctg_add_remove_items', true);
    $rc_wctg_choose_items   	= get_post_meta($post->ID, 'rc_wctg_choose_items', true);
    $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
    
    // Visibility
    $rc_wctg_public             = get_post_meta($post->ID, 'rc_wctg_public', true);
    $rc_wctg_show_ui            = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
    $rc_wctg_show_tagcloud      = get_post_meta($post->ID, 'rc_wctg_show_tagcloud', true); 
    $rc_wctg_show_admin_column  = get_post_meta($post->ID, 'rc_wctg_show_admin_column', true); 
    $rc_wctg_show_in_nav_menus  = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true); 

    // Options
    $rc_wctg_query_var          = get_post_meta($post->ID, 'rc_wctg_query_var', true); 
    $rc_wctg_rewrite            = get_post_meta($post->ID, 'rc_wctg_rewrite', true); 
    
    // Capabilities
    $rc_wctg_advanced_capabilities     = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
	$rc_wctg_capabilities_edit_terms   = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_terms', true);
	$rc_wctg_capabilities_manage_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_manage_terms', true);
	$rc_wctg_capabilities_delete_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_terms', true);
	$rc_wctg_capabilities_assign_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_assign_terms', true);

    
    ?>

    <?php if( isset($rc_wctg_mode) && $rc_wctg_mode == 'basic') : ?>    
    <script language="javascript">
	    jQuery(document).ready(function($) {
			$("#rc_wctg_general").show();
			$("#rc_wctg_features").hide();
			$("#rc_wctg_labels").hide();
			$("#rc_wctg_visibility").hide();
			$("#rc_wctg_options").hide();
			$("#rc_wctg_capabilities").hide();
			
			$("#rc_wctg_next_button_features").hide();
			
			$("#rc_wctg_tab_labels").hide();
			$("#rc_wctg_tab_visibility").hide();
			$("#rc_wctg_tab_options").hide();
			$("#rc_wctg_tab_capabilities").hide();
			
		});
    </script>
    <?php endif; ?>
    
<ol class="rc_wctg_step_tracker" data-rc_wctg_step_tracker-steps="5">
    <li class="rc_wctg_step_tracker-done" id="rc_wctg_tab_general">1 &rsaquo; <a href="#"><?php _e('General', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_labels">2 &rsaquo; <a href="#"><?php _e('Labels', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_visibility">3 &rsaquo; <a href="#"><?php _e('Visibility', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_options">4 &rsaquo; <a href="#"><?php _e('Options', 'rc_wctg'); ?></a></li><!--
 --><li class="rc_wctg_step_tracker-todo" id="rc_wctg_tab_capabilities">5 &rsaquo; <a href="#"><?php _e('Capabilities', 'rc_wctg'); ?></a></li>
</ol>

	<?php
	/*
	|--------------------------------------------------------------------------
	| GENERAL
	|--------------------------------------------------------------------------
	*/
	?>
    <div id="rc_wctg_general" class="rc_wctg_general">
    
	    <h4><?php _e('STEP', 'rc_wctg'); ?> 1: <?php _e('GENERAL PARAMETERS', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_taxonomy_fields">
	        <tr>
	            <td>
	            	<?php _e('Taxonomy Key', 'rc_wctg'); ?><span class="required">*</span>:<br/> <input type="text" name="rc_wctg_taxonomy_name" id="rc_wctg_taxonomy_name" value="<?php echo $rc_wctg_taxonomy_name; ?>"/>
	            	<span class="description"><?php _e('The Taxonomy key, ex: type', 'rc_wctg'); ?></span>
	            </td>
	            <td>
	            	<?php _e('Singular Label', 'rc_wctg'); ?><span class="required">*</span>:<br/> <input type="text" name="rc_wctg_singular_name" id="rc_wctg_singular_name" value="<?php echo $rc_wctg_singular_name; ?>"/>
	            	<span class="description"><?php _e('The singular name for your taxonomy, ex: Type', 'rc_wctg'); ?></span>
	            </td>
	            <td>
	            	<?php _e('Plural Label', 'rc_wctg'); ?><span class="required">*</span>:<br/> <input type="text" name="rc_wctg_plural_name" id="rc_wctg_plural_name" value="<?php echo $rc_wctg_plural_name; ?>"/>
	            	<span class="description"><?php _e('The plural name for your taxonomy, ex: Types', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="3">
	            	<?php _e('Description', 'rc_wctg'); ?>:<br/> <textarea name="rc_wctg_description" /><?php echo $rc_wctg_description; ?></textarea>
	            	<span class="description"><?php _e('A short descriptive summary of what the taxonomy is', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="3"><input type="checkbox" name="rc_wctg_hierarchical" <?php checked( $rc_wctg_hierarchical, 'on' ); ?>/> <?php _e('Hierarchical', 'rc_wctg'); ?> <span class="description"><?php _e('Whether the taxonomy is hierarchical (arranged in a tree-like structure). If checked, then a parent can be specified for each taxonomy.', 'rc_wctg'); ?></span></td>
	        </tr>
	        <tr>
	            <td colspan="3">
	            	<?php _e('Link to', 'rc_wctg'); ?>:<br />
	            	<?php $post_types = get_post_types('','names'); ?>
		            <select name="rc_wctg_link_to[]" id="rc_wctg_link_to" multiple="" id="rc_wctg_link_to" class="">
		            <?php
		            $k = 0;
		            if( $rc_wctg_link_to == '' ) { $rc_wctg_link_to = array(); }
		            foreach ($post_types as $post_type ) {
					  if( $post_type != 'cpt' && $post_type != 'metabox' && $post_type != 'revision' && $post_type != 'nav_menu_item' && $post_type != 'taxonomy' ) {
					  	?>
					  	<option <?php selected( in_array( $post_type, $rc_wctg_link_to ), true ); ?>><?php echo $post_type; ?></option>
					  	<?php
					  	$k++;
					  }
					}
					?>
					</select>
					<span class="description"><?php _e('Choose which post types to link the taxonomy to. Use the "Ctrl" or the "Cmd" key on your keyboard to select multiple custom post types.', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	    </table>
	    
	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_next_button_general" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
    </div>
    
	<?php
	/*
	|--------------------------------------------------------------------------
	| Labels
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_labels" class="rc_wctg_labels">

	    <h4><?php _e('STEP', 'rc_wctg'); ?> 2: <?php _e('LABELS', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_taxonomy_fields">
	        <tr>
	            <td><?php _e('Menu Text', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_menu_text" id="rc_wctg_menu_text" value="<?php echo $rc_wctg_menu_text; ?>"/></td>
	            <td><?php _e('All Items', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_all_items" id="rc_wctg_all_items" value="<?php echo $rc_wctg_all_items; ?>"/></td>
	            <td><?php _e('Edit Item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_edit_item" id="rc_wctg_edit_item" value="<?php echo $rc_wctg_edit_item; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('View Item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_view_item" id="rc_wctg_view_item" value="<?php echo $rc_wctg_view_item; ?>"/></td>
	        	<td><?php _e('Update Item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_update_item" id="rc_wctg_update_item" value="<?php echo $rc_wctg_update_item; ?>"/></td>
	            <td><?php _e('Add new item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_add_new_item" id="rc_wctg_add_new_item" value="<?php echo $rc_wctg_add_new_item; ?>"/></td>
	        </tr>
	        <tr>
	        	<td><?php _e('New Item name', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_new_item_name" id="rc_wctg_new_item_name" value="<?php echo $rc_wctg_new_item_name; ?>"/></td>
	            <td><?php _e('Parent item', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_parent_item" id="rc_wctg_parent_item" value="<?php echo $rc_wctg_parent_item; ?>"/></td>
	            <td><?php _e('Parent item Column', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_parent_item_colon" id="rc_wctg_parent_item_colon" value="<?php echo $rc_wctg_parent_item_colon; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Search Items', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_search_items" id="rc_wctg_search_items" value="<?php echo $rc_wctg_search_items; ?>"/></td>
	            <td><?php _e('Popular Items', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_popular_items" id="rc_wctg_popular_items" value="<?php echo $rc_wctg_popular_items; ?>"/></td>
	            <td><?php _e('Separate Items with commas', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_separate_items" id="rc_wctg_separate_items" value="<?php echo $rc_wctg_separate_items; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Add or remove items', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_add_remove_items" id="rc_wctg_add_remove_items" value="<?php echo $rc_wctg_add_remove_items; ?>"/></td>
	            <td><?php _e('Choose from most used', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_choose_items" id="rc_wctg_choose_items" value="<?php echo $rc_wctg_choose_items; ?>"/></td>
	            <td><?php _e('Not Found', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_not_found" id="rc_wctg_not_found" value="<?php echo $rc_wctg_not_found; ?>"/></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Labels are texts used for each action related to the taxonomy', 'rc_wctg'); ?></span>
	
	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_labels" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    	<input type="button" id="rc_wctg_next_button_labels" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
	</div>
	
	<?php
	/*
	|--------------------------------------------------------------------------
	| Visibility
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_visibility" class="rc_wctg_visibility">

	    <h4><?php _e('STEP', 'rc_wctg'); ?> 3: <?php _e('VISIBILITY', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_taxonomy_fields">
	        <tr>
	            <td><input type="checkbox" name="rc_wctg_public" <?php checked( $rc_wctg_public, 'on' ); ?>/> <?php _e('Public', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_show_ui" <?php checked( $rc_wctg_show_ui, 'on' ); ?>/> <?php _e('Show UI', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_show_tagcloud" <?php checked( $rc_wctg_show_tagcloud, 'on' ); ?>/> <?php _e('Show Tag Cloud', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_show_admin_column" <?php checked( $rc_wctg_show_admin_column, 'on' ); ?>/> <?php _e('Show Admin Column', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_show_in_nav_menus" <?php checked( $rc_wctg_show_in_nav_menus, 'on' ); ?>/> <?php _e('Show in Nav Menus', 'rc_wctg'); ?></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('Define if the taxonomy is public or not and if it should be integrated in the main UI', 'rc_wctg'); ?></span>
    
	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_visibility" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    	<input type="button" id="rc_wctg_next_button_visibility" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
	</div>
	
	<?php
	/*
	|--------------------------------------------------------------------------
	| Options
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_options" class="rc_wctg_options">

	    <h4><?php _e('STEP', 'rc_wctg'); ?> 4: <?php _e('OPTIONS', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_taxonomy_fields">
	        <tr>
	            <td><input type="checkbox" name="rc_wctg_query_var" <?php checked( $rc_wctg_query_var, 'on' ); ?>/> <?php _e('Query Var', 'rc_wctg'); ?></td>
	            <td><input type="checkbox" name="rc_wctg_rewrite" <?php checked( $rc_wctg_rewrite, 'on' ); ?>/> <?php _e('Rewrite', 'rc_wctg'); ?></td>
	        </tr>
	    </table>
	    
	    <span class="description"><?php _e('These are advanced options, if you are not familiar with them, simply leave blank, and click next', 'rc_wctg'); ?></span>

	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_options" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    	<input type="button" id="rc_wctg_next_button_options" class="button button-primary button-large" value="<?php _e('Next', 'rc_wctg'); ?>">
	    </div>
	</div>
	
	<?php
	/*
	|--------------------------------------------------------------------------
	| Capabilities
	|--------------------------------------------------------------------------
	*/
	?>
	<div id="rc_wctg_capabilities" class="rc_wctg_capabilities">

	    <h4><?php _e('ADVANCED CAPABILITIES', 'rc_wctg'); ?> 5:<?php _e('CAPABILITIES', 'rc_wctg'); ?></h4>
	    
	    <table width="100%" class="rc_wctg_taxonomy_fields">
			<td><input type="checkbox" name="rc_wctg_advanced_capabilities" id="rc_wctg_advanced_capabilities" <?php checked( $rc_wctg_advanced_capabilities, 'on' ); ?>/> <?php _e('Use Advanced Capabilities', 'rc_wctg'); ?></td>
		</table>
		
		<span class="description"><?php _e('Check to define your own capabilities for the taxonomy.', 'rc_wctg'); ?></span>
		
		<h4><?php _e('CUSTOM CAPABILITIES', 'rc_wctg'); ?></h4>
		
	    <table width="100%" class="rc_wctg_taxonomy_fields">
	        <tr>
	            <td><?php _e('Edit Terms', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_edit_terms" id="rc_wctg_capabilities_edit_terms" value="<?php echo $rc_wctg_capabilities_edit_terms; ?>"/></td>
	            <td><?php _e('Manage Terms', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_manage_terms" id="rc_wctg_capabilities_manage_terms" value="<?php echo $rc_wctg_capabilities_manage_terms; ?>"/></td>
	            <td><?php _e('Delete Terms', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_delete_terms" id="rc_wctg_capabilities_delete_terms" value="<?php echo $rc_wctg_capabilities_delete_terms; ?>"/></td>
	        </tr>
	        <tr>
	            <td><?php _e('Assign Terms', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_capabilities_assign_terms" id="rc_wctg_capabilities_assign_terms" value="<?php echo $rc_wctg_capabilities_assign_terms; ?>"/></td>
	        </tr>
	    </table>

	    <div class="rc_wctg_prev_next_buttons">
	    	<input type="button" id="rc_wctg_previous_button_capabilities" class="button button button-large" value="<?php _e('Previous', 'rc_wctg'); ?>">
	    </div>
	</div>
    

    <input type="hidden" name="rc_wctg_submit" value="true" />
    <?php
}


function rc_wctg_taxonomy_export_metabox() {
    global $post;
    
        /*
        |--------------------------------------------------------------------------
        | General
        |--------------------------------------------------------------------------
        */
	    $rc_wctg_taxonomy_name      = get_post_meta($post->ID, 'rc_wctg_taxonomy_name', true);
	    $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
	    $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
	    $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
	    $rc_wctg_link_to       		= get_post_meta($post->ID, 'rc_wctg_link_to', true);

        $rc_wctg_hierarchical = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        if ($rc_wctg_hierarchical == "on") {
            $rc_wctg_hierarchical = 'true';
        } else {
            $rc_wctg_hierarchical = 'false';
        }
        
        /*
        |--------------------------------------------------------------------------
        | LABELS
        |--------------------------------------------------------------------------
        */
        $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);
        $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
        $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
        $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
        $rc_wctg_update_item        = get_post_meta($post->ID, 'rc_wctg_update_item', true);
        $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
        $rc_wctg_new_item_name      = get_post_meta($post->ID, 'rc_wctg_new_item_name', true);
        $rc_wctg_parent_item  		= get_post_meta($post->ID, 'rc_wctg_parent_item', true);
        $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
        $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
        $rc_wctg_popular_items   	= get_post_meta($post->ID, 'rc_wctg_popular_items', true);
        $rc_wctg_separate_items   	= get_post_meta($post->ID, 'rc_wctg_separate_items', true);
        $rc_wctg_add_remove_items   = get_post_meta($post->ID, 'rc_wctg_add_remove_items', true);
        $rc_wctg_choose_items   	= get_post_meta($post->ID, 'rc_wctg_choose_items', true);
        $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
        
        /*
        |--------------------------------------------------------------------------
        | VISIBILITY
        |--------------------------------------------------------------------------
        */
        $rc_wctg_public = get_post_meta($post->ID, 'rc_wctg_public', true);
        if ($rc_wctg_public == "on") {
            $rc_wctg_public = 'true';
        } else {
            $rc_wctg_public = 'false';
        }
        $rc_wctg_show_ui = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
        if ($rc_wctg_show_ui == "on") {
            $rc_wctg_show_ui = 'true';
        } else {
            $rc_wctg_show_ui = 'false';
        }
        $rc_wctg_show_tagcloud = get_post_meta($post->ID, 'rc_wctg_show_tagcloud', true);
        if ($rc_wctg_show_tagcloud == "on") {
            $rc_wctg_show_tagcloud = 'true';
        } else {
            $rc_wctg_show_tagcloud = 'false';
        }
        $rc_wctg_show_admin_column = get_post_meta($post->ID, 'rc_wctg_show_admin_column', true);
        if ($rc_wctg_show_admin_column == "on") {
            $rc_wctg_show_admin_column = 'true';
        } else {
            $rc_wctg_show_admin_column = 'false';
        }
        $rc_wctg_show_in_nav_menus = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true);
        if ($rc_wctg_show_in_nav_menus == "on") {
            $rc_wctg_show_in_nav_menus = 'true';
        } else {
            $rc_wctg_show_in_nav_menus = 'false';
        }
        
        /*
        |--------------------------------------------------------------------------
        | OPTIONS
        |--------------------------------------------------------------------------
        */
        $rc_wctg_query_var = get_post_meta($post->ID, 'rc_wctg_query_var', true);
        if ($rc_wctg_query_var == "on") {
            $rc_wctg_query_var = 'true';
        } else {
            $rc_wctg_query_var = 'false';
        }
        $rc_wctg_rewrite = get_post_meta($post->ID, 'rc_wctg_rewrite', true);
        if ($rc_wctg_rewrite == "on") {
            $rc_wctg_rewrite = 'true';
        } else {
            $rc_wctg_rewrite = 'false';
        }
        
        /*
        |--------------------------------------------------------------------------
        | Capabilities
        |--------------------------------------------------------------------------
        */
	    $rc_wctg_advanced_capabilities     = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
		$rc_wctg_capabilities_edit_terms   = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_terms', true);
		$rc_wctg_capabilities_manage_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_manage_terms', true);
		$rc_wctg_capabilities_delete_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_terms', true);
		$rc_wctg_capabilities_assign_terms = get_post_meta($post->ID, 'rc_wctg_capabilities_assign_terms', true);

        if ($rc_wctg_advanced_capabilities == "on") {
        
        	$rc_wctg_capabilities = array(
							            'edit_terms'   => $rc_wctg_capabilities_edit_terms,
							            'manage_terms' => $rc_wctg_capabilities_manage_terms,
							            'delete_terms' => $rc_wctg_capabilities_delete_terms,
							            'assign_terms' => $rc_wctg_capabilities_assign_terms
        							);
        } else {
	        $rc_wctg_capabilities = array();
        }
    
    
        _e('If you need to export this custom taxonomy to another WordPress install, or use still use the taxonomy when the "Content Manager" plugin is deactivated, simply copy and paste the code below in the "functions.php" file in your theme folder.', 'rc_wctg');
        
		echo "<pre>";
		
        // Create Labels
        echo "
/**
 * Register '$rc_wctg_taxonomy_name' taxonomy
 *
 * Generated by WordPress Content Types Generator
 *
 * @Version     ".RC_WCTG_PLUGIN_VERSION." 
 * @Author      Remi Corson
 * @URL		http://remicorson.com
*/      

// Taxonomy Labels      
&#36labels = array(
	'name'                       => _x( '$rc_wctg_taxonomy_name', 'taxonomy general name' ),
	'singular_name'              => _x( '$rc_wctg_singular_name', 'taxonomy singular name' ),
	'search_items'               => __( '$rc_wctg_search_items' ),
	'popular_items'              => __( '$rc_wctg_popular_items' ),
	'all_items'                  => __( '$rc_wctg_all_items' ),
	'parent_item'                => __( '$rc_wctg_parent_item' ),
	'parent_item_colon'          => __( '$rc_wctg_parent_item_colon' ),
	'edit_item'                  => __( '$rc_wctg_edit_item' ),
	'view_item'                  => __( '$rc_wctg_view_item' ),
	'update_item'                => __( '$rc_wctg_update_item' ),
	'add_new_item'               => __( '$rc_wctg_add_new_item' ),
	'new_item_name'              => __( '$rc_wctg_new_item_name' ),
	'add_or_remove_items'        => __( '$rc_wctg_add_remove_items' ),
	'choose_from_most_used'      => __( '$rc_wctg_choose_items' ),
	'separate_items_with_commas' => __( '$rc_wctg_separate_items' ),
	'menu_name'                  => __( '$rc_wctg_menu_text' ),
);
";

if( isset($rc_wctg_advanced_capabilities) && $rc_wctg_advanced_capabilities == 'on') {          
        // Create Capabilities
        echo "
// Taxonomy Capabilities  
&#36capabilities = array(
    'edit_terms'   => '$rc_wctg_capabilities_edit_terms',
    'manage_terms' => '$rc_wctg_capabilities_manage_terms',
    'delete_terms' => '$rc_wctg_capabilities_delete_terms',
    'assign_terms' => '$rc_wctg_capabilities_assign_terms'
);
";
}

        // Create CPT
        $count_cpt = count($rc_wctg_link_to);
        $j = 1;
        echo "
// Linked Custom Post Types
&#36cpts = array(";
    foreach( $rc_wctg_link_to as $rc_wctg_cpt ) {
        echo "'$rc_wctg_cpt'";
        if( $j < $count_cpt )
        	echo ', ';
        $j++;
    }
echo ");
";
        
        // Create CPT arguments
        echo "
// Taxonomy Arguments  
&#36args = array(
    'labels'             => &#36labels,
    'hierarchical'       => $rc_wctg_hierarchical,
    'description'        => '$rc_wctg_description',
    'public'             => $rc_wctg_public,
    'show_ui'            => $rc_wctg_show_ui,
    'show_tagcloud'      => $rc_wctg_show_tagcloud,
    'show_in_nav_menus'  => $rc_wctg_show_in_nav_menus,
    'show_admin_column'  => $rc_wctg_show_admin_column,
    'query_var'          => $rc_wctg_query_var,
    'rewrite'            => $rc_wctg_rewrite,";
    
// Advanced capabilities
if( isset($rc_wctg_advanced_capabilities) && $rc_wctg_advanced_capabilities == 'on') {
	$echo_args .= "
    'capabilities' 	 => &#36capabilities,";
}

echo $echo_args;
echo "
);
";

echo "register_taxonomy( '$rc_wctg_taxonomy_name', &#36cpts, &#36args );";

		echo "</pre>";
    
}

function rc_wctg_taxonomy_mode_metabox() {
    global $post;
    
    // Mode
    $rc_wctg_mode = get_post_meta($post->ID, 'rc_wctg_mode', true);
    
    ?>
    <p><?php _e('If you are bit lost with this form, you can enable "basic" mode. By enabling this mode you will get rid of advanced fields.', 'rc_wctg'); ?>
    </p>
    <table width="100%" class="rc_wctg_taxonomy_fields">
    	<tr>
	    	<td><input id="rc_wctg_mode_expert" type="radio" name="rc_wctg_mode" value="expert" <?php checked( $rc_wctg_mode, 'expert' ); ?>> <?php _e('Expert', 'rc_wctg'); ?></td>
	    	<td><input id="rc_wctg_mode_basic" type="radio" name="rc_wctg_mode" value="basic" <?php checked( $rc_wctg_mode, 'basic' ); ?>> <?php _e('Basic', 'rc_wctg'); ?></td>
    	</tr>
    </table>
    <?php
    
}

function rc_wctg_load_taxonomy_metaboxes() {
    add_meta_box('cpt_metabox', __('Custom Post Type Settings', 'rc_wctg'), 'rc_wctg_taxonomy_metabox', 'taxonomy', 'normal');
    add_meta_box('cpt_code_metabox', __('Export', 'rc_wctg'), 'rc_wctg_taxonomy_export_metabox', 'taxonomy', 'normal');
    add_meta_box('cpt_mode_metabox', __('Edition Mode', 'rc_wctg'), 'rc_wctg_taxonomy_mode_metabox', 'taxonomy', 'side');
}


add_action('save_post', 'rc_wctg_taxonomy_save_postdata');
add_action('add_meta_boxes', 'rc_wctg_load_taxonomy_metaboxes');
