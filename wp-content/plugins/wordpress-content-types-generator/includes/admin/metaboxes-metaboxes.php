<?php

function rc_wctg_metabox_save_postdata() {

    global $post;
    
    if ( isset($_POST['rc_wctg_submit']) && $_POST['rc_wctg_submit'] == 'true') {
    	        
    	// General
        $rc_wctg_metabox_name       = get_post_meta($post->ID, 'rc_wctg_metabox_name', true);
        $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
        $rc_wctg_link_to	        = get_post_meta($post->ID, 'rc_wctg_link_to', true);
        $rc_wctg_context	        = get_post_meta($post->ID, 'rc_wctg_context', true);
        $rc_wctg_priority	        = get_post_meta($post->ID, 'rc_wctg_priority', true);
        $rc_wctg_ids	     	    = get_post_meta($post->ID, 'rc_wctg_ids', true);
        $rc_wctg_metabox_fields	    = get_post_meta($post->ID, 'rc_wctg_metabox_fields', true);
        
        // General
        if( isset( $_POST['rc_wctg_metabox_name'] ) ) { update_post_meta($post->ID, 'rc_wctg_metabox_name', sanitize_title($_POST['rc_wctg_metabox_name']), $rc_wctg_metabox_name); }
        if( isset( $_POST['rc_wctg_description'] ) ) { update_post_meta($post->ID, 'rc_wctg_description', $_POST['rc_wctg_description'], $rc_wctg_description); }
        if( isset( $_POST['rc_wctg_link_to'] ) ) { update_post_meta($post->ID, 'rc_wctg_link_to', $_POST['rc_wctg_link_to'], $rc_wctg_link_to); }
        if( isset( $_POST['rc_wctg_context'] ) ) { update_post_meta($post->ID, 'rc_wctg_context', $_POST['rc_wctg_context'], $rc_wctg_context); }
        if( isset( $_POST['rc_wctg_priority'] ) ) { update_post_meta($post->ID, 'rc_wctg_priority', $_POST['rc_wctg_priority'], $rc_wctg_priority); }
        if( isset( $_POST['rc_wctg_ids'] ) ) { update_post_meta($post->ID, 'rc_wctg_ids', $_POST['rc_wctg_ids'], $rc_wctg_ids); }
        
        // Fields
		if( isset( $_POST['rc_wctg_metabox_fields'] ) ) { update_post_meta($post->ID, 'rc_wctg_metabox_fields', $_POST['rc_wctg_metabox_fields'], $rc_wctg_metabox_fields); }
    }
}

function rc_wctg_metabox_metabox() {
    global $post;
	        
	// General
    $rc_wctg_metabox_name       = get_post_meta($post->ID, 'rc_wctg_metabox_name', true);
    $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
    $rc_wctg_link_to       		= get_post_meta($post->ID, 'rc_wctg_link_to', true);
    $rc_wctg_context	        = get_post_meta($post->ID, 'rc_wctg_context', true);
    $rc_wctg_priority	        = get_post_meta($post->ID, 'rc_wctg_priority', true);
    $rc_wctg_ids	       		= get_post_meta($post->ID, 'rc_wctg_ids', true);

	/*
	|--------------------------------------------------------------------------
	| GENERAL
	|--------------------------------------------------------------------------
	*/
	?>
    <div id="rc_wctg_general" class="rc_wctg_general">
	    
	    <table width="100%" class="rc_wctg_metabox_fields">
	        <tr>
	            <td>
	            	<?php _e('Metabox Name', 'rc_wctg'); ?><span class="required">*</span>:<br/> <input type="text" name="rc_wctg_metabox_name" id="rc_wctg_metabox_name" value="<?php echo $rc_wctg_metabox_name; ?>"/>
	            	<span class="description"><?php _e('The metabox name.', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="3">
	            	<?php _e('Description', 'rc_wctg'); ?>:<br/> <textarea name="rc_wctg_description" /><?php echo $rc_wctg_description; ?></textarea>
	            	<span class="description"><?php _e('A short descriptive summary of what the metabox is', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	        <tr>
	            <td colspan="3">
	            	<?php _e('Link to', 'rc_wctg'); ?>:<br />
	            	<?php $post_types = get_post_types('','names'); ?>
		            <select name="rc_wctg_link_to[]" id="rc_wctg_link_to" multiple="" id="rc_wctg_link_to" class="">
		            <?php
		            $k = 0;
		            if( $rc_wctg_link_to == '' ) { $rc_wctg_link_to = array(); }
		            foreach ($post_types as $post_type ) {
					  if( $post_type != 'cpt' && $post_type != 'metabox' && $post_type != 'revision' && $post_type != 'nav_menu_item' && $post_type != 'taxonomy' ) {
					  	?>
					  	<option <?php selected( in_array( $post_type, $rc_wctg_link_to ), true ); ?>><?php echo $post_type; ?></option>
					  	<?php
					  	$k++;
					  }
					}
					?>
					</select>
					<br />
					<span class="description"><?php _e('Choose which post types to link the metabox to. Use the "Ctrl" or the "Cmd" key on your keyboard to select multiple custom post types.', 'rc_wctg'); ?></span>
	            </td>
	        </tr>
	        <tr>	
	        	<td>
					<?php _e('Context', 'rc_wctg'); ?>: 
					<select name="rc_wctg_context">
						<option value="">-- <?php _e('choose', 'rc_wctg'); ?> --</option>
						<option value="normal" <?php selected( $rc_wctg_context, 'normal' ); ?>><?php _e('normal', 'rc_wctg'); ?></option>
						<option value="advanced" <?php selected( $rc_wctg_context, 'advanced' ); ?>><?php _e('advanced', 'rc_wctg'); ?></option>
						<option value="side" <?php selected( $rc_wctg_context, 'side' ); ?>><?php _e('side', 'rc_wctg'); ?></option>
					</select>
				</td>
	        	<td>
					<?php _e('Priority', 'rc_wctg'); ?>: 
					<select name="rc_wctg_priority">
						<option value="">-- <?php _e('choose', 'rc_wctg'); ?> --</option>
						<option value="default" <?php selected( $rc_wctg_priority, 'default' ); ?>><?php _e('default', 'rc_wctg'); ?></option>
						<option value="high" <?php selected( $rc_wctg_priority, 'high' ); ?>><?php _e('high', 'rc_wctg'); ?></option>
						<option value="core" <?php selected( $rc_wctg_priority, 'core' ); ?>><?php _e('core', 'rc_wctg'); ?></option>
						<option value="low" <?php selected( $rc_wctg_priority, 'low' ); ?>><?php _e('low', 'rc_wctg'); ?></option>
					</select>
				</td>
	            <!-- <td><?php _e('Posts / Pages IDs', 'rc_wctg'); ?>:<br/> <input type="text" name="rc_wctg_ids" id="rc_wctg_ids" value="<?php echo $rc_wctg_ids; ?>"/></td> -->
	        </tr>
	    </table>

    </div>    

    <input type="hidden" name="rc_wctg_submit" value="true" />
    <?php
}

function rc_wctg_metabox_fields_metabox() {
    global $post;
    
	echo '<div class="rc_wctg_repeatable_fields">';
	

    $rc_wctg_metabox_fields	= get_post_meta($post->ID, 'rc_wctg_metabox_fields', true);
	
	$type_options = array(array(1,'Text'), array(2,'Textarea'), array(3,'Checkbox'), array(4,'Select'), array(5,'Radio'));
	$placeholders = array();
	
	if ( !$rc_wctg_metabox_fields )
	{
		$default_name        = "";
		$default_id          = "";
		$default_description = "";
		$default_type        = 1;
		$default_options     = "";
		$rc_wctg_metabox_fields = array(array($default_name,$default_id,$default_description,$default_type,$default_options));
	}
	
	if ( $rc_wctg_metabox_fields ) {
		$i = 0;
		foreach ($rc_wctg_metabox_fields as $single_field)
		{
			if ( !empty($single_field) )
			{
				echo '<p>';
					echo '<span class="rc_wctg_move_repeatable_field rc_wctg_repeatable_move"></span>';
					
					echo '<input class="" placeholder="'.__('Name', 'rc_wctg').'" type="text" size="20" name="rc_wctg_metabox_fields['.$i.'][]" value="'.$single_field[0].'"  />';
					echo '<input class="rc_wctg_metabox_ids_fields" placeholder="'.__('ID', 'rc_wctg').'" type="text" size="20" name="rc_wctg_metabox_fields['.$i.'][]" value="'.$single_field[1].'" />';
					echo '<input class="" placeholder="'.__('Description', 'rc_wctg').'" type="text" size="20" name="rc_wctg_metabox_fields['.$i.'][]" value="'.$single_field[2].'" />';
					echo '<select name="rc_wctg_metabox_fields['.$i.'][]">';
					foreach ($type_options as $type)
					{
						$value = $type[0];
						$label = $type[1];
						$selected = '';
						
						if ($single_field[3] == $value) $selected = 'selected="selected"';
						
						echo '<option '. $selected .' value="'.$value.'">'.__($label, 'rc_wctg').'</option>';
					}
					echo '</select>';
					echo '<input class="" placeholder="'.__('Options', 'rc_wctg').'" type="text" size="20" name="rc_wctg_metabox_fields['.$i.'][]" value="'.$single_field[4].'" />';
					echo '<span class="rc_wctg_remove_repeatable_field button">' . __('Remove', 'rc_wctg') . '</span>';
				echo '</p>';
			}
			++$i;
		}
	}
		
	echo '</div>';
	echo '<span class="rc_wctg_add_repeatable_field button-primary">' . __('Add Field', 'rc_wctg') . '</span>';
	echo '<br /><br /><span class="description">' . __('Click on "Add Field" to add a new custom field to the metabox your editing.', 'rc_wctg') . '</span>';

}

function rc_wctg_metabox_export_metabox() {
    global $post;
    
        /*
        |--------------------------------------------------------------------------
        | General
        |--------------------------------------------------------------------------
        */
        $rc_wctg_metabox_name       = get_post_meta($post->ID, 'rc_wctg_metabox_name', true);
        $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
        $rc_wctg_link_to	        = get_post_meta($post->ID, 'rc_wctg_link_to', true);
        $rc_wctg_context	        = get_post_meta($post->ID, 'rc_wctg_context', true);
        $rc_wctg_priority	        = get_post_meta($post->ID, 'rc_wctg_priority', true);
        $rc_wctg_ids	     	    = get_post_meta($post->ID, 'rc_wctg_ids', true);
        
    
    
        _e('If you need to export this custom metabox to another WordPress install, or use still use the metabox when the "Content Manager" plugin is deactivated, simply copy and paste the code below in the "functions.php" file in your theme folder.', 'rc_wctg');
        
		echo "<pre>";
	    
	    // Get Fields
	    $rc_wctg_metabox_fields	= get_post_meta($post->ID, 'rc_wctg_metabox_fields', true);
	    
	    // Create table
	    $table = "";
		$table .= "<style>\n";
		$table .= "	#rc_wctg_metabox th {\n";
		$table .= "		width: 150px;\n";
		$table .= "	}\n";
		$table .= "	#rc_wctg_metabox input[type=\"text\"], #rc_wctg_metabox textarea {\n";
		$table .= "		padding: 4px 8px;\n";
		$table .= "		border: 1px solid #ccc;\n";
		$table .= "		width: 400px;\n";
		$table .= "	}\n";
		$table .= "	#side-sortables .form-table th {\n";
		$table .= "		width: 60px !important;\n";
		$table .= "	}\n";
		$table .= "	#side-sortables #rc_wctg_metabox input[type=\"text\"], #side-sortables #rc_wctg_metabox textarea {\n";
		$table .= "		width: 100%;\n";
		$table .= "	}\n";
		$table .= "	#rc_wctg_metabox input[type=\"checkbox\"] {\n";
		$table .= "		float: left;\n";
		$table .= "		margin: 8px 8px 0 0;\n";
		$table .= "		width: 20px;\n";
		$table .= "	}\n";
		$table .= "	</style>\n\n";
		$table .= "	<table class=\"form-table\" id=\"rc_wctg_metabox\">\n";
		$table .= "		<tbody>\n";					
	
		foreach( $rc_wctg_metabox_fields as $field ) {
		
			// Get field data
			$field_name        = $field[0];
			$field_id          = $field[1];
			$field_description = $field[2];
			$field_type        = $field[3];
			$field_options     = $field[4];
			
			$dollar = html_entity_decode('$');
			
			// Table fields labels
			$table .= "		<tr class=\"form-field\">\n";
			$table .= "			<th scope=\"row\" valign=\"top\">\n";
			$table .= "				<label for=\"$field_id\">$field_name</label>\n";
			$table .= "			</th>\n";
	
			switch( $field_type ) {
				// Text Field
				case 1:
					$sanitized_field_id = str_replace('-','_',$field_id);
					$table .= "			<?php ".$dollar."$sanitized_field_id = ( get_post_meta(".$dollar."post->ID, '$field_id', true) ) ? get_post_meta(".$dollar."post->ID, '$field_id', true) : ''; ?>\n";
					$table .= "			<td>\n";
					$table .= "				<input type=\"text\" id=\"$field_id\" name=\"rc_wctg_meta_field[$field_id]\" value=\"<?php echo ".$dollar."$sanitized_field_id; ?>\" title=\"$field_description\">\n";
					$table .= "				<span class=\"description\">$field_description</span>\n";
					$table .= "			</td>\n";
				    break;
				// Textarea
				case 2:
					$sanitized_field_id = str_replace('-','_',$field_id);
					$table .= "			<?php ".$dollar."$sanitized_field_id = ( get_post_meta(".$dollar."post->ID, '$field_id', true) ) ? get_post_meta(".$dollar."post->ID, '$field_id', true) : ''; ?>\n";
					$table .= "			<td>\n";
					$table .= "				<textarea id=\"$field_id\" name=\"rc_wctg_meta_field[$field_id]\" title=\"$field_description\"><?php echo ".$dollar."$sanitized_field_id; ?></textarea>\n";
					$table .= "				<p class=\"description\">$field_description</p>\n";
					$table .= "			</td>\n";
				    break;
				// Checkbox
				case 3:

					$table .= "			<td>\n";
					$table .= "				<input type=\"hidden\" id=\"$field_id\" name=\"rc_wctg_meta_field[$field_id]\" value=\"\">\n";
					$table .= "				<input type=\"checkbox\" id=\"$field_id\" name=\"rc_wctg_meta_field[$field_id]\" title=\"$field_description\" <?php checked( get_post_meta(".$dollar."post->ID, '$field_id', true), 'on' ); ?>>\n";
					$table .= "				<p class=\"description\">$field_description</p>\n";
					$table .= "			</td>\n";
				    break;
				// Dropdown Select
				case 4:
					$table .= "			<td>\n";
					$table .= "			<select name=\"rc_wctg_meta_field[$field_id]\" id=\"$field_id\">\n"; 
					$options = explode(',', $field_options);
				    foreach ($options as $option) {  
				        $table .= "			<option value=\"$option\" <?php selected( get_post_meta(".$dollar."post->ID, '$field_id', true), '$option' ); ?>>$option</option>\n";  
				    } 
				    $table .= "			</select><br /><p class=\"description\">$field_description</p>\n";
					$table .= "			</td>\n";
				    break;
				// Radio
				case 5:
					$table .= "			<td>\n";
					$options = explode(',', $field_options);
					foreach ( $options as $option ) { 
					    $table .= "			<input type=\"radio\" name=\"rc_wctg_meta_field[$field_id]\" id=\"$field_id\" value=\"$option\" style=\"width: 20px;\" /> $option<br />\n"; 
					}  
				    $table .= "			<p class=\"description\">$field_description</p>\n";
					$table .= "			</td>\n";
				    break;
				default:
					$table .= __('Field type is not set.', 'rc_wctg');
					break;
			}
			
		}
		
		// Table end
		$table .= "		</tr>\n";
		$table .= "	</tbody>\n";
		$table .= "	</table>\n";
		
		$cleaned_table = htmlentities($table);
		
		$rc_wctg_metabox_name = str_replace('-','_',$rc_wctg_metabox_name);
		
        // Create add_meta_box
        $count_link_to = count($rc_wctg_link_to);
        $k = 0;

	    foreach( $rc_wctg_link_to as $rc_wctg_link ) {
	        $link_to .= "add_meta_box('$rc_wctg_metabox_name', '$rc_wctg_description', 'rc_wctg_get_metabox_".$rc_wctg_metabox_name."_fields', '$rc_wctg_link', '$rc_wctg_context', '$rc_wctg_priority');
        ";
	        if( $k < $count_link_to)
	        $k++;
	    }
		
        // Create export code
        echo "
/**
 * Register '$rc_wctg_metabox_name' metabox
 *
 * Generated by WordPress Content Types Generator
 *
 * @Version     ".RC_WCTG_PLUGIN_VERSION." 
 * @Author      Remi Corson
 * @URL		http://remicorson.com
*/     

// Registers the '$rc_wctg_metabox_name' metabox
function rc_wctg_add_".$rc_wctg_metabox_name."_metabox() {
	$link_to
}
add_action('add_meta_boxes', 'rc_wctg_add_".$rc_wctg_metabox_name."_metabox'); 

// Creates the '$rc_wctg_metabox_name' metabox content
function rc_wctg_get_metabox_".$rc_wctg_metabox_name."_fields() {
	
	global &#36post;
	
	?>
	$cleaned_table
	&lt;?php
}

// Creates the 'save' function
// ---------------------------------------------------------------------------------
// NOTE: this function must be used only ONCE
//       If you declare this function more than once you will get an error message
// ---------------------------------------------------------------------------------

function rc_wctg_save_post_meta(&#36post_id, &#36post) {

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', &#36post->ID ))
		return &#36post->ID;
		
	&#36post_meta =  array();
	
	// place all meta fields into a single array
	if( isset(&#36_POST['rc_wctg_meta_field'] ) ) {
		&#36meta_fields = &#36_POST['rc_wctg_meta_field'];
		foreach( &#36meta_fields as &#36meta_key => &#36meta_value ) {
			&#36post_meta[&#36meta_key] = &#36meta_value;
		}
	}
	
	// Add values of &#36post_meta as custom fields
	foreach (&#36post_meta as &#36key => &#36value) {
		if( &#36post->post_type == 'revision' ) return;
		&#36value = implode(',', (array)&#36value);
		if(get_post_meta(&#36post->ID, &#36key, FALSE)) {
			update_post_meta(&#36post->ID, &#36key, &#36value);
		} else {
			add_post_meta(&#36post->ID, &#36key, &#36value);
		}
		if(!&#36value) delete_post_meta(&#36post->ID, &#36key);
	}

}

add_action('save_post', 'rc_wctg_save_post_meta', 1, 2); // save the custom fields
";

		echo "</pre>";
    
}


function rc_wctg_load_metabox_metaboxes() {
    add_meta_box('cpt_metabox', __('Metabox Settings', 'rc_wctg'), 'rc_wctg_metabox_metabox', 'metabox', 'normal');
    add_meta_box('cpt_fields_metabox', __('Custom Fields', 'rc_wctg'), 'rc_wctg_metabox_fields_metabox', 'metabox', 'normal');
    add_meta_box('cpt_code_metabox', __('Export', 'rc_wctg'), 'rc_wctg_metabox_export_metabox', 'metabox', 'normal');
}


add_action('save_post', 'rc_wctg_metabox_save_postdata');
add_action('add_meta_boxes', 'rc_wctg_load_metabox_metaboxes');
