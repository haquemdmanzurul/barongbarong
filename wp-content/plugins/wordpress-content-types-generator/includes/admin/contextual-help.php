<?php
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

function rc_wctg_contextual_help( $contextual_help, $screen_id, $screen ) {

	$screen = get_current_screen();
	//$contextual_help .= var_dump($screen); // use this to help determine $screen->id

	// CPT
	if ('cpt' == $screen->id ) {
	
		// General
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_cpt_help_general',
			'title'	    => __( 'General', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'A custom post type is a specific kind of content that you can add to your WordPress install. To create a custom post type (also known as CPT), you have to enter a name, a singular and a plural label. Note that the name must not contain space and special characters. By security if you do not follow this rule every space and special character will be automatically removed. If you are not very familiar with the CPT creation, switch to the basic mode. It should be much easier for you.', 'rc_wctg' ) . '</p>'
		) );
		
		// Features
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_cpt_help_features',
			'title'	    => __( 'Features', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Supported features (also known as "supports") are globally elements that you can add to custom post types. You can for example choose to enable or disable each element depending on your needs. All supports are native WordPress features. Tags and categories: ou can choose to use already existing categories, add tags support or page categories to your custom post types.', 'rc_wctg' ) . '</p>'
		) );
		
		// Labels
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_cpt_help_labels',
			'title'	    => __( 'Labels', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Labels are automatically set in the above fields, by the way you can define your own custom labels. You can for example shorten them if they are too long. These fields cannot be empty.', 'rc_wctg' ) . '</p>'
		) );	
			
		// Visibility
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_cpt_help_visibility',
			'title'	    => __( 'Visibility', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Most part of the time you will create public CPT, that means CPT that are viewable by your visitors. But if you want to use a specific CPT for private use, do not make it public, and only connected users will be able to access their data through the admin console.', 'rc_wctg' ) . '</p>'
		) );
					
		// Options
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_cpt_help_options',
			'title'	    => __( 'Options', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'These options are advanced options. If you do not know what they are exactly, we recommend you not to use them. In doubt you can check the Codex.', 'rc_wctg' ) . '</p>'
		) );
	
	}
	
	// Taxonomy
	if ('taxonomy' == $screen->id ) {
		
		// General
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_taxonomy_help_general',
			'title'	    => __( 'General', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Taxonomies are a way to group or categorize objects in WordPress, such as posts, and custom post types. If you are not very familiar with the taxonomies creation, switch to the basic mode. It should be much easier for you.', 'rc_wctg' ) . '</p>'
		) );
		
		// Labels
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_taxonomy_help_labels',
			'title'	    => __( 'Labels', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Labels are automatically set in the above fields, by the way you can define your own custom labels. You can for example shorten them if they are too long. These fields cannot be empty.', 'rc_wctg' ) . '</p>'
		) );	
			
		// Visibility
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_taxonomy_help_visibility',
			'title'	    => __( 'Visibility', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Most part of the time you will create public taxonomies, that means taxonomies that are viewable by your visitors.', 'rc_wctg' ) . '</p>'
		) );
					
		// Options
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_taxonomy_help_options',
			'title'	    => __( 'Options', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'These options are advanced options. If you do not know what they are exactly, we recommend you not to use them. In doubt you can check the Codex.', 'rc_wctg' ) . '</p>'
		) );
		
					
		// Capabilities
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_taxonomy_help_capabilities',
			'title'	    => __( 'Capabilities', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Capabilities are sort of rights that you can give to your users. You can decide who can edit, manage, delete and assign taxonomies terms.', 'rc_wctg' ) . '</p>'
		) );
	
	}
	
	// Metabox
	if ('metabox' == $screen->id ) {
	
		$screen->add_help_tab( array(
			'id'	    => 'rc_wctg_metabox_help',
			'title'	    => __( 'General', 'rc_wctg' ),
			'content'	=> '<p>' . __( 'Metaboxes are boxes located on the posts, pages, and CPTs addition and edition pages. They can contain custom fields. You can create as many metaboxes as you want and as many custom fields by metaboxes. You can define on which elements to show the metaboxes. Content of custom fields can easily be retrieved using builtin WordPress functions.', 'rc_wctg' ) . '</p>'
		) );
	
	}

	// Common sidebar
	$screen->set_help_sidebar(
	'<p><strong>' . __('For more information:') . '</strong></p>' .
	'<p><a href="http://remicorson.com" target="_blank">' .__( 'Author Website', 'rc_wctg' ). '</a></p>' .
	'<p><a href="http://remicorson.com/support/" target="_blank">' . __('Free Support Forums', 'rc_wctg') . '</a></p>'
	);

}
 
add_action( 'contextual_help', 'rc_wctg_contextual_help', 10, 3 );