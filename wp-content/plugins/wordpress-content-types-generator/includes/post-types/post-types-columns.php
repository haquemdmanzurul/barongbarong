<?php

/*
|--------------------------------------------------------------------------
| cpts Columns
|--------------------------------------------------------------------------
*/

/**
 * Load custom CPT columns
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_cpt_columns($columns)
{

	$newcolumns = array(
		"cb"       	  => "<input type  = \"checkbox\" />",
		"title"       => "",
		"labels"      => esc_html__('Labels', 'rc_wctg'),
		"slug"        => esc_html__('Slug', 'rc_wctg'),
		"description" => esc_html__('Description', 'rc_wctg'),
	);

	
	$columns = array_merge($newcolumns, $columns);
	
	return $columns;
}


/**
 * Charge CPT columns content
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_cpt_columns_content($column)
{
	global $post;
	
	$cpt_id = $post->ID;
	
	// CPT data
	$rc_wctg_cpt_name           = get_post_meta($post->ID, 'rc_wctg_cpt_name', true);
    $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
    $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
    $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);

	switch ($column)
	{
		case "slug":
			echo $rc_wctg_cpt_name;
		break;	
		case "labels":
			echo __('Singular', 'rc_wctg').': '.$rc_wctg_singular_name.'<br />'.__('Plural', 'rc_wctg').': '.$rc_wctg_plural_name.'';
		break;	
		case "description":
			echo $rc_wctg_description;
		break;	
		
	}
}


/**
 * Load CPTs columns
 *
 * @access      public
 * @since       1.0 
 * @return      
*/
function rc_wctg_load_cpts_columns() {
	
	global $typenow;
	
	if( $typenow == 'cpt' ) {
		add_filter("manage_edit-cpt_columns", "rc_wctg_cpt_columns");
		add_action("manage_posts_custom_column",  "rc_wctg_cpt_columns_content");
	}
}

add_action("admin_init",  "rc_wctg_load_cpts_columns");