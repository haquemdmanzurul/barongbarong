<?php

/**
 * Setup cpt Post Type
 *
 * Registers the cpt CPT.
 *
 * @since       1.0
 * @return      void
*/

function rc_wctg_setup_post_types() {

	$archives = true;
	if( defined( 'rc_wctg_DISABLE_ARCHIVE' ) && rc_wctg_DISABLE_ARCHIVE == true ) {
		$archives = false;
	}

	/* Register cpt post type */

	$cpt_labels =  apply_filters( 'rc_wctg_cpt_labels', array(
		'name'                => '%2$s',
		'singular_name'       => '%1$s',
		'add_new'             => __('Add New', 'rc_wctg'),
		'add_new_item'        => __('Add New %1$s', 'rc_wctg'),
		'edit_item'           => __('Edit %1$s', 'rc_wctg'),
		'new_item'            => __('New %1$s', 'rc_wctg'),
		'all_items'           => __('%2$s', 'rc_wctg'),
		'view_item'           => __('View %1$s', 'rc_wctg'),
		'search_items'        => __('Search %2$s', 'rc_wctg'),
		'not_found'           => __('No %2$s found', 'rc_wctg'),
		'not_found_in_trash'  => __('No %2$s found in Trash', 'rc_wctg'),
		'parent_item_colon'   => '',
		'menu_name'           => __('%2$s', 'rc_wctg'),
		'exclude_from_search' => true
	) );

	foreach ( $cpt_labels as $key => $value ) {
	   $cpt_labels[ $key ] = sprintf( $value, rc_wctg_get_cpts_label_singular(), rc_wctg_get_cpts_label_plural() );
	}

	$cpt_args = array(
		'labels'             => $cpt_labels,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'wordpress-post-types-manager',
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => $archives,
		'hierarchical'       => false,
		'supports' 			 => apply_filters('rc_wctg_cpt_supports', array( 'title' ) ),
	);
	register_post_type( 'cpt', apply_filters( 'rc_wctg_cpt_post_type_args', $cpt_args ) );
	
	/* Register taxonomy post type */

	$taxonomy_labels =  apply_filters( 'rc_wctg_taxonomy_labels', array(
		'name'                => '%2$s',
		'singular_name'       => '%1$s',
		'add_new'             => __('Add New', 'rc_wctg'),
		'add_new_item'        => __('Add New %1$s', 'rc_wctg'),
		'edit_item'           => __('Edit %1$s', 'rc_wctg'),
		'new_item'            => __('New %1$s', 'rc_wctg'),
		'all_items'           => __('%2$s', 'rc_wctg'),
		'view_item'           => __('View %1$s', 'rc_wctg'),
		'search_items'        => __('Search %2$s', 'rc_wctg'),
		'not_found'           => __('No %2$s found', 'rc_wctg'),
		'not_found_in_trash'  => __('No %2$s found in Trash', 'rc_wctg'),
		'parent_item_colon'   => '',
		'menu_name'           => __('%2$s', 'rc_wctg'),
		'exclude_from_search' => true
	) );

	foreach ( $taxonomy_labels as $key => $value ) {
	   $taxonomy_labels[ $key ] = sprintf( $value, rc_wctg_get_taxonomies_label_singular(), rc_wctg_get_taxonomies_label_plural() );
	}

	$taxonomy_args = array(
		'labels'             => $taxonomy_labels,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'wordpress-post-types-manager',
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => $archives,
		'hierarchical'       => false,
		'supports' 			 => apply_filters('rc_wctg_taxonomy_supports', array( 'title' ) ),
	);
	register_post_type( 'taxonomy', apply_filters( 'rc_wctg_taxonomy_post_type_args', $taxonomy_args ) );
	
	/* Register metabox post type */

	$metabox_labels =  apply_filters( 'rc_wctg_metabox_labels', array(
		'name'                => '%2$s',
		'singular_name'       => '%1$s',
		'add_new'             => __('Add New', 'rc_wctg'),
		'add_new_item'        => __('Add New %1$s', 'rc_wctg'),
		'edit_item'           => __('Edit %1$s', 'rc_wctg'),
		'new_item'            => __('New %1$s', 'rc_wctg'),
		'all_items'           => __('%2$s', 'rc_wctg'),
		'view_item'           => __('View %1$s', 'rc_wctg'),
		'search_items'        => __('Search %2$s', 'rc_wctg'),
		'not_found'           => __('No %2$s found', 'rc_wctg'),
		'not_found_in_trash'  => __('No %2$s found in Trash', 'rc_wctg'),
		'parent_item_colon'   => '',
		'menu_name'           => __('%2$s', 'rc_wctg'),
		'exclude_from_search' => true
	) );

	foreach ( $metabox_labels as $key => $value ) {
	   $metabox_labels[ $key ] = sprintf( $value, rc_wctg_get_metaboxes_label_singular(), rc_wctg_get_metaboxes_label_plural() );
	}

	$metabox_args = array(
		'labels'             => $metabox_labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'wordpress-post-types-manager',
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => $archives,
		'hierarchical'       => false,
		'supports' 			 => apply_filters('rc_wctg_metabox_supports', array( 'title' ) ),
	);
	register_post_type( 'metabox', apply_filters( 'rc_wctg_metabox_post_type_args', $metabox_args ) );
	
}

add_action('init', 'rc_wctg_setup_post_types');

/**
 * Get Default cpts Label
 *
 * @access      public
 * @since       1.0.8.3
 * @return      array
*/

function rc_wctg_get_default_cpts_labels() {
	$defaults = array(
	   'singular' => __('Content Type','rc_wctg'),
	   'plural'   => __('Content Types','rc_wctg')
	);
	return apply_filters( 'rc_wctg_get_default_cpts_name', $defaults );
}

/**
 * Get Default taxonomy Label
 *
 * @access      public
 * @since       1.0.8.3
 * @return      array
*/

function rc_wctg_get_default_taxonomies_labels() {
	$defaults = array(
	   'singular' => __('Custom Taxonomy','rc_wctg'),
	   'plural'   => __('Custom Taxonomies','rc_wctg')
	);
	return apply_filters( 'rc_wctg_get_default_taxonomies_name', $defaults );
}

/**
 * Get Default metabox Label
 *
 * @access      public
 * @since       1.0.8.3
 * @return      array
*/

function rc_wctg_get_default_metaboxes_labels() {
	$defaults = array(
	   'singular' => __('Custom Metabox','rc_wctg'),
	   'plural'   => __('Metaboxes & fields','rc_wctg')
	);
	return apply_filters( 'rc_wctg_get_default_metaboxes_name', $defaults );
}


/**
 * Get Label Singular
 *
 * @access      public
 * @since       1.0.8.3
 * @return      string
*/

function rc_wctg_get_cpts_label_singular( $lowercase = false ) {
	$defaults = rc_wctg_get_default_cpts_labels();
	return ($lowercase) ? strtolower( $defaults['singular'] ) : $defaults['singular'];
}

/**
 * Get taxonomies Singular
 *
 * @access      public
 * @since       1.0.8.3
 * @return      string
*/

function rc_wctg_get_taxonomies_label_singular( $lowercase = false ) {
	$defaults = rc_wctg_get_default_taxonomies_labels();
	return ($lowercase) ? strtolower( $defaults['singular'] ) : $defaults['singular'];
}

/**
 * Get metaboxes Singular
 *
 * @access      public
 * @since       1.0.8.3
 * @return      string
*/

function rc_wctg_get_metaboxes_label_singular( $lowercase = false ) {
	$defaults = rc_wctg_get_default_metaboxes_labels();
	return ($lowercase) ? strtolower( $defaults['singular'] ) : $defaults['singular'];
}


/**
 * Get CPT Plural
 *
 * @access      public
 * @since       1.0.8.3
 * @return      string
*/

function rc_wctg_get_cpts_label_plural( $lowercase = false ) {
	$defaults = rc_wctg_get_default_cpts_labels();
	return ( $lowercase ) ? strtolower( $defaults['plural'] ) : $defaults['plural'];
}

/**
 * Get taxonomies Plural
 *
 * @access      public
 * @since       1.0.8.3
 * @return      string
*/

function rc_wctg_get_taxonomies_label_plural( $lowercase = false ) {
	$defaults = rc_wctg_get_default_taxonomies_labels();
	return ( $lowercase ) ? strtolower( $defaults['plural'] ) : $defaults['plural'];
}

/**
 * Get metaboxes Plural
 *
 * @access      public
 * @since       1.0.8.3
 * @return      string
*/

function rc_wctg_get_metaboxes_label_plural( $lowercase = false ) {
	$defaults = rc_wctg_get_default_metaboxes_labels();
	return ( $lowercase ) ? strtolower( $defaults['plural'] ) : $defaults['plural'];
}

/**
 * Change default "enter title here" input
 *
 * @access      public
 * @since       1.4.0.2
 * @return      string
*/

function rc_wctg_change_default_title( $title ){
     $screen = get_current_screen();
 
     if  ( 'cpt' == $screen->post_type ) {
     	$label = rc_wctg_get_cpts_label_singular();
        $title = sprintf( __('Enter %s title here', 'rc_wctg'), $label);
     }
 
     return $title;
}
 
add_filter( 'enter_title_here', 'rc_wctg_change_default_title' );

/**
 * Remove un-needed metaboxes
 *
 * @access      public
 * @since       1.4.0.2
 * @return      string
*/

function rc_wctg_remove_metaboxes() {
	
	// cpts
	remove_meta_box( 'commentstatusdiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'commentsdiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'cpt_statusdiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'cpt_labeldiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'cpt_milestonediv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'cpt_prioritydiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'authordiv' , 'cpt' , 'normal' ); 
	
	remove_meta_box( 'rc_wctg_statusdiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'rc_wctg_typediv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'rc_wctg_citydiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'rc_wctg_countydiv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'rc_wctg_zipcodediv' , 'cpt' , 'normal' ); 
	remove_meta_box( 'rc_wctg_statediv' , 'cpt' , 'normal' ); 

}
add_action( 'admin_menu' , 'rc_wctg_remove_metaboxes' );