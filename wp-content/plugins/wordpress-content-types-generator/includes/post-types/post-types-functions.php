<?php

/**
 * Register custom post types
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
function rc_wctg_init_custom_post_types() {

	// Create the Query
	$posts_per_page = -1;
	$post_type 		= 'cpt';
				
	$query = new WP_Query( array ( 
								'post_type' => $post_type,
								'posts_per_page' => $posts_per_page,
								) 
						);
	
	//Get post type count
	$post_count = $query->post_count;
	$i = 1;
	
		// Loop
		while ($query->have_posts()) : $query->the_post();
		
        global $post;
        
        /*
        |--------------------------------------------------------------------------
        | Mode
        |--------------------------------------------------------------------------
        */
        $rc_wctg_mode           	= get_post_meta($post->ID, 'rc_wctg_mode', true);

        
        /*
        |--------------------------------------------------------------------------
        | General
        |--------------------------------------------------------------------------
        */
        $rc_wctg_cpt_name           = get_post_meta($post->ID, 'rc_wctg_cpt_name', true);
        $rc_wctg_singular_name      = get_post_meta($post->ID, 'rc_wctg_singular_name', true);
        $rc_wctg_plural_name        = get_post_meta($post->ID, 'rc_wctg_plural_name', true);
        $rc_wctg_description        = get_post_meta($post->ID, 'rc_wctg_description', true);
        $rc_wctg_hierarchical       = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        
        /*
        |--------------------------------------------------------------------------
        | FEATURES
        |--------------------------------------------------------------------------
        */
        $rc_wctg_s = array();
        $rc_wctg_taxos = array();
        
        $rc_wctg_s_title = get_post_meta($post->ID, 'rc_wctg_s_title', true);
        if ($rc_wctg_s_title == "on") {
            $rc_wctg_s[] = 'title';
        }
        $rc_wctg_s_editor = get_post_meta($post->ID, 'rc_wctg_s_editor', true);
        if ($rc_wctg_s_editor == "on") {
            $rc_wctg_s[] = 'editor';
        }
        $rc_wctg_s_author = get_post_meta($post->ID, 'rc_wctg_s_author', true);
        if ($rc_wctg_s_author == "on") {
            $rc_wctg_s[] = 'author';
        }
        $rc_wctg_s_thumbnail = get_post_meta($post->ID, 'rc_wctg_s_thumbnail', true);
        if ($rc_wctg_s_thumbnail == "on") {
            $rc_wctg_s[] = 'thumbnail';
        }
        $rc_wctg_s_excerpt = get_post_meta($post->ID, 'rc_wctg_s_excerpt', true);
        if ($rc_wctg_s_excerpt == "on") {
            array_push($rc_wctg_s, 'excerpt');
        }
        $rc_wctg_s_trackbacks = get_post_meta($post->ID, 'rc_wctg_s_trackbacks', true);
        if ($rc_wctg_s_trackbacks == "on") {
            array_push($rc_wctg_s, 'trackbacks');
        }
        $rc_wctg_s_custom_fields = get_post_meta($post->ID, 'rc_wctg_s_custom_fields', true);
        if ($rc_wctg_s_custom_fields == "on") {
            array_push($rc_wctg_s, 'custom-fields');
        }
        $rc_wctg_s_comments = get_post_meta($post->ID, 'rc_wctg_s_comments', true);
        if ($rc_wctg_s_comments == "on") {
            array_push($rc_wctg_s, 'comments');
        }
        $rc_wctg_s_revisions = get_post_meta($post->ID, 'rc_wctg_s_revisions', true);
        if ($rc_wctg_s_revisions == "on") {
            array_push($rc_wctg_s, 'revisions');
        }
        $rc_wctg_s_post_formats = get_post_meta($post->ID, 'rc_wctg_s_post_formats', true);
        if ($rc_wctg_s_post_formats == "on") {
            array_push($rc_wctg_s, 'post-formats');
        }
        
        $rc_wctg_categories = get_post_meta($post->ID, 'rc_wctg_categories', true);
        if ($rc_wctg_categories == "on") {
            $rc_wctg_taxos[] = 'category';
        }
        $rc_wctg_tags = get_post_meta($post->ID, 'rc_wctg_tags', true);
        if ($rc_wctg_tags == "on") {
            $rc_wctg_taxos[] = 'post_tag';
        }
        $rc_wctg_page_categories = get_post_meta($post->ID, 'rc_wctg_page_categories', true);
        if ($rc_wctg_page_categories == "on") {
            $rc_wctg_taxos[] = 'page-category';
        }
        
        /*
        |--------------------------------------------------------------------------
        | LABELS
        |--------------------------------------------------------------------------
        */

        $rc_wctg_add_new            = get_post_meta($post->ID, 'rc_wctg_add_new', true);
        $rc_wctg_add_new_item       = get_post_meta($post->ID, 'rc_wctg_add_new_item', true);
        $rc_wctg_edit_item          = get_post_meta($post->ID, 'rc_wctg_edit_item', true);
        $rc_wctg_new_item           = get_post_meta($post->ID, 'rc_wctg_new_item', true);
        $rc_wctg_all_items          = get_post_meta($post->ID, 'rc_wctg_all_items', true);
        $rc_wctg_view_item          = get_post_meta($post->ID, 'rc_wctg_view_item', true);
        $rc_wctg_search_items       = get_post_meta($post->ID, 'rc_wctg_search_items', true);
        $rc_wctg_not_found          = get_post_meta($post->ID, 'rc_wctg_not_found', true);
        $rc_wctg_not_found_in_trash = get_post_meta($post->ID, 'rc_wctg_not_found_in_trash', true);
        $rc_wctg_parent_item_colon  = get_post_meta($post->ID, 'rc_wctg_parent_item_colon', true);
        $rc_wctg_menu_text  		= get_post_meta($post->ID, 'rc_wctg_menu_text', true);
        
        /*
        |--------------------------------------------------------------------------
        | VISIBILITY
        |--------------------------------------------------------------------------
        */
        $rc_wctg_public = get_post_meta($post->ID, 'rc_wctg_public', true);
        if ($rc_wctg_public == "on") {
            $rc_wctg_public = true;
        } else {
            $rc_wctg_public = false;
        }
        $rc_wctg_show_ui = get_post_meta($post->ID, 'rc_wctg_show_ui', true);
        if ($rc_wctg_show_ui == "on") {
            $rc_wctg_show_ui = true;
        } else {
            $rc_wctg_show_ui = false;
        }
        $rc_wctg_show_in_menu = get_post_meta($post->ID, 'rc_wctg_show_in_menu', true);
        if ($rc_wctg_show_in_menu == "on") {
            $rc_wctg_show_in_menu = true;
        } else {
            $rc_wctg_show_in_menu = false;
        }
        $rc_wctg_show_in_admin_bar = get_post_meta($post->ID, 'rc_wctg_show_in_admin_bar', true);
        if ($rc_wctg_show_in_admin_bar == "on") {
            $rc_wctg_show_in_admin_bar = true;
        } else {
            $rc_wctg_show_in_admin_bar = false;
        }
        $rc_wctg_exclude_from_search = get_post_meta($post->ID, 'rc_wctg_exclude_from_search', true);
        if ($rc_wctg_exclude_from_search == "on") {
            $rc_wctg_exclude_from_search = true;
        } else {
            $rc_wctg_exclude_from_search = false;
        }
        $rc_wctg_show_in_nav_menus = get_post_meta($post->ID, 'rc_wctg_show_in_nav_menus', true);
        if ($rc_wctg_show_in_nav_menus == "on") {
            $rc_wctg_show_in_nav_menus = true;
        } else {
            $rc_wctg_show_in_nav_menus = false;
        }
        
        $rc_wctg_show_in_menu_val  	= get_post_meta($post->ID, 'rc_wctg_show_in_menu_val', true);
        $rc_wctg_show_in_admin_bar  = get_post_meta($post->ID, 'rc_wctg_show_in_admin_bar', true);
        $rc_wctg_menu_position  	= get_post_meta($post->ID, 'rc_wctg_menu_position', true);
        $rc_wctg_menu_icon      	= get_post_meta($post->ID, 'rc_wctg_menu_icon', true);
        
		if( isset($rc_wctg_menu_icon) && $rc_wctg_menu_icon == '' ) {
			$rc_wctg_menu_icon = RC_WCTG_PLUGIN_URL."assets/images/default_cpt_icon.png";
		}
        
        // Show in menu val
        if( isset( $rc_wctg_show_in_menu_val ) && $rc_wctg_show_in_menu_val != '' )
        	$rc_wctg_show_in_menu = $rc_wctg_show_in_menu_val;
        
        /*
        |--------------------------------------------------------------------------
        | OPTIONS
        |--------------------------------------------------------------------------
        */
        $rc_wctg_publicly_queryable = get_post_meta($post->ID, 'rc_wctg_publicly_queryable', true);
        if ($rc_wctg_publicly_queryable == "on") {
            $rc_wctg_publicly_queryable = true;
        } else {
            $rc_wctg_publicly_queryable = false;
        }
        $rc_wctg_query_var = get_post_meta($post->ID, 'rc_wctg_query_var', true);
        if ($rc_wctg_query_var == "on") {
            $rc_wctg_query_var = true;
        } else {
            $rc_wctg_query_var = false;
        }
        $rc_wctg_rewrite = get_post_meta($post->ID, 'rc_wctg_rewrite', true);
        if ($rc_wctg_rewrite == "on") {
            $rc_wctg_rewrite = true;
        } else {
            $rc_wctg_rewrite = false;
        }
        $rc_wctg_has_archive = get_post_meta($post->ID, 'rc_wctg_has_archive', true);
        if ($rc_wctg_has_archive == "on") {
            $rc_wctg_has_archive = true;
        } else {
            $rc_wctg_has_archive = false;
        }
        $rc_wctg_hierarchical = get_post_meta($post->ID, 'rc_wctg_hierarchical', true);
        if ($rc_wctg_hierarchical == "on") {
            $rc_wctg_hierarchical = true;
        } else {
            $rc_wctg_hierarchical = false;
        }
        $rc_wctg_can_export = get_post_meta($post->ID, 'rc_wctg_can_export', true);
        if ($rc_wctg_can_export == "on") {
            $rc_wctg_can_export = true;
        } else {
            $rc_wctg_can_export = false;
        }
        
        /*
        |--------------------------------------------------------------------------
        | Capabilities
        |--------------------------------------------------------------------------
        */
        $rc_wctg_capability_type          		 = get_post_meta($post->ID, 'rc_wctg_capability_type', true);
        $rc_wctg_advanced_capabilities           = get_post_meta($post->ID, 'rc_wctg_advanced_capabilities', true);
        $rc_wctg_capabilities_edit_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_post', true);
        $rc_wctg_capabilities_edit_posts         = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_posts', true);
        $rc_wctg_capabilities_edit_others_posts  = get_post_meta($post->ID, 'rc_wctg_capabilities_edit_others_posts', true);
        $rc_wctg_capabilities_publish_posts      = get_post_meta($post->ID, 'rc_wctg_capabilities_publish_posts', true);
        $rc_wctg_capabilities_read_post          = get_post_meta($post->ID, 'rc_wctg_capabilities_read_post', true);
        $rc_wctg_capabilities_read_private_posts = get_post_meta($post->ID, 'rc_wctg_capabilities_read_private_posts', true);
        $rc_wctg_capabilities_delete_post        = get_post_meta($post->ID, 'rc_wctg_capabilities_delete_post', true);

        if ($rc_wctg_advanced_capabilities == "on") {
        
        	$rc_wctg_capabilities = array(
							            'edit_post'          => $rc_wctg_capabilities_edit_post,
							            'edit_posts'         => $rc_wctg_capabilities_edit_posts,
							            'edit_others_posts'  => $rc_wctg_capabilities_edit_others_posts,
							            'publish_posts'      => $rc_wctg_capabilities_publish_posts,
							            'read_post'          => $rc_wctg_capabilities_read_post,
							            'read_private_posts' => $rc_wctg_capabilities_read_private_posts,
							            'delete_post'        => $rc_wctg_capabilities_delete_post
        							);
        }
        
        /*
        |--------------------------------------------------------------------------
        | Register Post Type
        |--------------------------------------------------------------------------
        */

        // Create Labels
        $labels = array(
            'name'               => _x( $rc_wctg_plural_name, 'post type general name' ),
            'singular_name'      => _x( $rc_wctg_singular_name, 'post type singular name' ),
            'add_new'            => _x( $rc_wctg_add_new, $rc_wctg_cpt_name ),
            'add_new_item'       => __( $rc_wctg_add_new_item ),
            'edit_item'          => __( $rc_wctg_edit_item ),
            'new_item'           => __( $rc_wctg_new_item ),
            'all_items'          => __( $rc_wctg_all_items ),
            'view_item'          => __( $rc_wctg_view_item ),
            'search_items'       => __( $rc_wctg_search_items ),
            'not_found'          => __( $rc_wctg_not_found ),
            'not_found_in_trash' => __( $rc_wctg_not_found_in_trash ),
            'parent_item_colon'  => __( $rc_wctg_parent_item_colon ),
            'menu_name'          => __( $rc_wctg_menu_text )
        );
        
        // Create CPT arguments
        $args = array(
            'labels'             => $labels,
            'hierarchical'       => $rc_wctg_hierarchical,
            'description'        => $rc_wctg_description,
            'public'             => $rc_wctg_public,
            'publicly_queryable' => $rc_wctg_publicly_queryable,
            'show_ui'            => $rc_wctg_show_ui,
            'show_in_menu'       => $rc_wctg_show_in_menu,
            'menu_icon'          => $rc_wctg_menu_icon,
            'show_in_nav_menus'  => $rc_wctg_show_in_nav_menus,
            'show_in_admin_bar'  => $rc_wctg_show_in_admin_bar,
            'exclude_from_search'=> $rc_wctg_exclude_from_search,
            'query_var'          => $rc_wctg_query_var,
            'rewrite'            => $rc_wctg_rewrite,
            'can_export'         => $rc_wctg_can_export,
            'capability_type'    => $rc_wctg_capability_type,
            'has_archive'        => $rc_wctg_has_archive,
            'menu_position'      => $rc_wctg_menu_position,
            'taxonomies' 		 => $rc_wctg_taxos,
            'supports'           => $rc_wctg_s
        );
        
		// Advanced capabilities
		if( isset($rc_wctg_advanced_capabilities) && $rc_wctg_advanced_capabilities == 'on') {
			$add_capabilities = array('capabilities' => $rc_wctg_capabilities);
			array_push($args, $add_capabilities);
		}

        
        register_post_type( $rc_wctg_cpt_name, $args );

    endwhile;
}

// Load Custom Post Types generated by the plugin
add_action('init', 'rc_wctg_init_custom_post_types');