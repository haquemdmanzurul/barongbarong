<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Load Scripts
 *
 * Enqueues the required scripts.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_load_scripts() {

	global $rc_wctg_options, $post;

	if( !is_admin() ) {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'rc_wctg_frontend_scripts', RC_WCTG_PLUGIN_URL . 'assets/js/frontend.js', array(), RC_WCTG_PLUGIN_VERSION );
	}

}
add_action( 'wp_enqueue_scripts', 'rc_wctg_load_scripts' );


/**
 * Register Styles
 *
 * Checks the styles option and hooks the required filter.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_register_styles() {
	global $rc_wctg_options;

	if( isset( $rc_wctg_options['disable_styles'] ) )
		return;

	wp_enqueue_style( 'rc_wctg_frontend', RC_WCTG_PLUGIN_URL . 'assets/css/styles.css', array(), RC_WCTG_PLUGIN_VERSION );

}
add_action( 'wp_enqueue_scripts', 'rc_wctg_register_styles' );


/**
 * Load Admin Scripts
 *
 * Enqueues the required admin scripts.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_load_admin_scripts( $hook ) {

	global $post, $pagenow, $rc_wctg_settings_page, $typenow;

	$rc_wctg_pages = array( $rc_wctg_settings_page, 'index.php' );
	$rc_wctg_cpt   = apply_filters( 'rc_wctg_load_scripts_for_these_types', array( 'cpt', 'taxonomy', 'metabox') );

	if ( !in_array( $hook, $rc_wctg_pages ) && !is_object( $post ) )
	    return;

	if ( is_object( $post ) && !in_array( $post->post_type, $rc_wctg_cpt ) )
	    return;

	// CPT
	if( $typenow == 'cpt' ) {
		if( $hook == $rc_wctg_settings_page OR $hook == 'post.php' OR $hook == 'edit.php' OR $hook == 'post-new.php' ) {
			wp_enqueue_style( 'rc_wctg_admin_settings_styles', RC_WCTG_PLUGIN_URL . 'assets/css/admin.css', array(), RC_WCTG_PLUGIN_VERSION );
		}
		
		if( $hook == $rc_wctg_settings_page OR $hook == 'post.php' OR $hook == 'edit.php' OR $hook == 'post-new.php' ) {
			wp_enqueue_script( 'rc_wctg_admin_settings_scripts', RC_WCTG_PLUGIN_URL . 'assets/js/add-edit-cpt.js', array(), RC_WCTG_PLUGIN_VERSION );
			
			// Media upload callback
			if ( function_exists( 'wp_enqueue_media' ) ){
			    wp_enqueue_media();
			} else {
			    wp_enqueue_style('thickbox');
			    wp_enqueue_script('media-upload');
			    wp_enqueue_script('thickbox');
			}
			
		}
	}
	
	// Taxonomy
	if( $typenow == 'taxonomy' ) {
		if( $hook == $rc_wctg_settings_page OR $hook == 'post.php' OR $hook == 'edit.php' OR $hook == 'post-new.php' ) {
			wp_enqueue_style( 'rc_wctg_admin_settings_styles', RC_WCTG_PLUGIN_URL . 'assets/css/admin.css', array(), RC_WCTG_PLUGIN_VERSION );
		}
		
		if( $hook == $rc_wctg_settings_page OR $hook == 'post.php' OR $hook == 'edit.php' OR $hook == 'post-new.php' ) {
			wp_enqueue_script( 'rc_wctg_admin_settings_scripts', RC_WCTG_PLUGIN_URL . 'assets/js/add-edit-taxonomy.js', array(), RC_WCTG_PLUGIN_VERSION );
		}
	}
	
	// Metabox
	if( $typenow == 'metabox' ) {
		if( $hook == $rc_wctg_settings_page OR $hook == 'post.php' OR $hook == 'edit.php' OR $hook == 'post-new.php' ) {
			wp_enqueue_style( 'rc_wctg_admin_settings_styles', RC_WCTG_PLUGIN_URL . 'assets/css/admin.css', array(), RC_WCTG_PLUGIN_VERSION );
		}
		
		if( $hook == $rc_wctg_settings_page OR $hook == 'post.php' OR $hook == 'edit.php' OR $hook == 'post-new.php' ) {
			wp_enqueue_script( 'rc_wctg_admin_settings_scripts', RC_WCTG_PLUGIN_URL . 'assets/js/add-edit-metabox.js', array(), RC_WCTG_PLUGIN_VERSION );
		}
	}
	
}
add_action( 'admin_enqueue_scripts', 'rc_wctg_load_admin_scripts', 100 );


/**
 * Admin Icon
 *
 * Echoes the CSS for the post type icon.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_admin_cpt_icon() {
    global $post_type;
	$icon_url = RC_WCTG_PLUGIN_URL . 'assets/images/post-type-cpt-icon.png';
	?>
	<style type="text/css" media="screen">
		#toplevel_page_wordpress-post-types-manager .wp-menu-image {
	            background: url(<?php echo RC_WCTG_PLUGIN_URL; ?>assets/images/post-type-cpt-icon.png) no-repeat 6px -17px !important;
	        }
		#toplevel_page_wordpress-post-types-manager:hover .wp-menu-image, #toplevel_page_wordpress-post-types-manager.wp-has-current-submenu .wp-menu-image {
	            background-position:6px 7px!important;
	        }

		<?php if ( ( isset( $_GET['post_type'] ) ) && ( $_GET['post_type'] == 'cpt' ) || ( $post_type == 'cpt' ) || ( $post_type == 'taxonomy' ) || ( $post_type == 'metabox' ) || ( $post_type == 'field' ) ) : ?>
        #icon-wordpress-post-types-manager { background:transparent url("<?php echo RC_WCTG_PLUGIN_URL . 'assets/images/post-type-cpt-icon-32.png'; ?>") no-repeat; }
        <?php endif; ?>
	</style>
    <?php
}
add_action( 'admin_head','rc_wctg_admin_cpt_icon' );