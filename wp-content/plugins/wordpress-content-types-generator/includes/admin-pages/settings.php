<?php
/**
 * Admin Options Page
*/


/**
 * Options Page
 *
 * Renders the options page contents.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_options_page() {

	global $rc_wctg_options;

	$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general';  

	ob_start(); ?>
	<div class="wrap">
		
		<h2 class="nav-tab-wrapper">
			<a href="<?php echo add_query_arg('tab', 'general', remove_query_arg('settings-updated')); ?>" class="nav-tab <?php echo $active_tab == 'general' ? 'nav-tab-active' : ''; ?>"><?php _e('General', 'rc_wctg'); ?></a>
		</h2>
			
		<div id="tab_container">
				
			<?php settings_errors( 'rc_rmb-notices' ); ?>

			<form method="post" action="options.php">
	
				<?php
				
				if($active_tab == 'general') {
					settings_fields('rc_wctg_settings_general'); 
					do_settings_sections('rc_wctg_settings_general');
				}
				
				submit_button(); 
				
				?>
				 
			</form>
		</div><!--end #tab_container-->
		
	</div>
	<?php
	echo ob_get_clean();
}