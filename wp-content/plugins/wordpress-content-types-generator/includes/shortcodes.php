<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Display meta fields data
 *
 * Allows the user to easily display the content of a post meta field
 *
 * @access      public
 * @since       1.1
 * @return      string
*/

function rc_wctg_field_shortcode( $atts, $content = null )
{
	global $post;
	
	extract( shortcode_atts( array(
			'id'   => '',
			'type' => ''
		), $atts )
	);
	
	// Get post meta field value
	$meta = get_post_meta($post->ID, $id, true);
	
	// Checkbox Field Type
	if( isset($type) && $type == 'checkbox' ) {
		if( $meta == 'on' ) {
			return __('Yes', 'rc_wctg');
		} else {
			return __('No', 'rc_wctg');
		}
	} else {
		return $meta;
	}

}
add_shortcode('wctg_field', 'rc_wctg_field_shortcode');
