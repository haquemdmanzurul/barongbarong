<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Deactivate
 *
 * Runs on plugin deactivation.
 *
 * @access      private
 * @since       1.1
 * @return      void
*/

function rc_wctg_deactivate() {

	global $wpdb;
	global $post;
	
	/* None for now */

}

register_deactivation_hook( RC_WCTG_BASE_FILE, 'rc_wctg_deactivate' );