<?php

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Install
 *
 * Runs on plugin install.
 *
 * @access      private
 * @since       1.0
 * @return      void
*/

function rc_wctg_installation() {

	global $wpdb;
	global $post;
	
	if ( !get_option( 'rc_wctg_is_installed' ) ) {
	
		// Setup Custom Post Types
		rc_wctg_setup_post_types();
	
		// Clear permalinks
		flush_rewrite_rules();

		// Add default options
		update_option( 'rc_wctg_is_installed', true );	

	}

}

register_activation_hook( RC_WCTG_BASE_FILE, 'rc_wctg_installation' );