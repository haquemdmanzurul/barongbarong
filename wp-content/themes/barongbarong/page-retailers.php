<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 
 Template Name: Retailers
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
	<div class="container-fluid map-container">
    <div id="map_canvas"></div>
		<?php //echo do_shortcode('[mappress mapid="1"]');
		
		//print_r(getRetailerGoogleMapPin());
		 ?>
        <script type="text/javascript"  src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCHkl6rIzKIevJVo-EErxr5B7ng2KUiEyg&sensor=false">  </script>

<script type="text/javascript">
var map;
 var markers = [];

// Add a Home control that returns the user to London
/*function HomeControl(controlDiv, map) {
  controlDiv.style.padding = '1px';
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = '3399cc';
  controlUI.style.border='1px solid';
  controlUI.style.cursor = 'pointer';
  controlUI.style.textAlign = 'left';
controlUI.style.width = '200px';
  controlUI.title = 'Set map to London';
  controlDiv.appendChild(controlUI);
  var controlText = document.createElement('div');
  controlText.style.fontFamily='Arial,sans-serif';
  controlText.style.fontSize='12px';*/
/*  controlText.style.paddingLeft = '4px';
  controlText.style.paddingRight = '4px';*/
  /*controlText.innerHTML = '<div class="content-header-blue"><div><div style="float: left;"><a href="javascript:void(0);" class="up" id="btn-slide-map-control"></a></div><div class="content-header-text" >Symbols</div></div></div><div class="content-part" id="panel-map-control" style="display: none"><div class="map-control-red">Urgent</div><div class="map-control-green">1-10 days long</div><div class="map-control-yellow">10+ days long</div><div class="map-control-purple">Multiple Vacancies</div><div class="map-control-grey">Not yet in varaope</div></div>';
  controlUI.appendChild(controlText);
*/
  // Setup click-event listener: simply set the map to London
 /* google.maps.event.addDomListener(controlUI, 'click', function() {
    //map.setCenter(london)
	
				$("#panel-map-control").slideToggle("slow");
                if($("#btn-slide-map-control").hasClass('up')){
                    $("#btn-slide-map-control").removeClass('up');
                    $("#btn-slide-map-control").addClass('down');
                } else {
                    $("#btn-slide-map-control").removeClass('down');
                    $("#btn-slide-map-control").addClass('up');
                }
                return false;
			
  });
}*/
function initialize() {
    var mapOptions = {
        zoom: 5,
        center: new google.maps.LatLng(51.437595, 5.4792164),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
		mapTypeControl: true,
		mapTypeControlOptions: {
			style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
			position: google.maps.ControlPosition.BOTTOM_CENTER
		},		
		streetViewControl: true,
		streetViewControlOptions: {
			position: google.maps.ControlPosition.TOP_LEFT
		}

		
    };
    map = new google.maps.Map(document.getElementById('map_canvas'),
            mapOptions);
	
  //var homeControlDiv = document.createElement('div');	
  //var homeControl = new HomeControl(homeControlDiv, map);
//  homeControlDiv.index = 1;
 
  //map.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);
    var locations = <?php echo getRetailerGoogleMapPin(); ?>;


    //var infowindow = new google.maps.InfoWindow();

    var marker;
	$.each(locations, function( index, markerobj ) {
	  console.log(markerobj.lat);
	  
	  marker = new google.maps.Marker({
		/* position: new google.maps.LatLng(locations[i][3], locations[i][4]),*/
		position: new google.maps.LatLng(markerobj.lat, markerobj.lon),
		map: map
      });
	  markers.push(marker);

	});

    }

// Sets the map on all markers in the array.
function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setAllMap(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
    </div>
    <div class='container-fluid'>
      <section class='row-fluid text-left' id='first'>
        <div class='col-md-12 text-center'>
          <h1><?php the_title(); ?> </h1>
        </div>
        <div class='col-md-4'>
          <div class='row'>
            <div class='col-md-12'>
               <?php
					global $wpdb;
					$countries_db = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."countries ORDER BY country ASC");
					$query_available_country = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."postmeta WHERE meta_key='country'");
					//print_r($query_available_country);
					$retailers_country = array();
					$retailers_city = array();
					if(count($query_available_country)>0){
						foreach($query_available_country as $c){
							$data =  maybe_unserialize($c->meta_value);
							if(!in_array($data['country_name'],$retailers_country))
							array_push($retailers_country,$data['country_name']);
							if(!in_array($data['city'],$retailers_city))
							array_push($retailers_city,$data['city_name']);							
						}
					}		  
			  ?>           
              <div class='select'>

                <select name="country" class="retailer_country">
                  <option>Select Country</option>
                  <?php

			
					$countries = array();
			
					foreach ($countries_db AS $country)
					{
						if (trim($country->country) == '') continue;
						if(!in_array($country->country,$retailers_country)) continue;
						echo '<option value="'.$country->id.'">'.$country->country.'</option>';
					}				  
				  
				  ?>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class='col-md-4'>
          <div class='row'>
            <div class='col-md-12'>
              <div class='select'>
                <select name="city" class="retailer_city">
                  <option>Select City</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class='col-md-4'>
          <div class='row'>
            <div class='col-md-12'>
              <div class='select'>
                <select>
                  <option>Select Type</option>
                  <option>#1</option>
                  <option>#2</option>
                  <option>#3</option>
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class='row retailer_container'>
          <?php
            $type = 'retailers';
            $args=array(
              'post_type' => $type,
              'post_status' => 'publish',
              'posts_per_page' => -1,
              'caller_get_posts'=> 1
              );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
              while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <div class='col-md-4 store'>
                   <div class='col-md-10'>
                    <h4>
                      <?php the_title(); ?>
                    </h4>
                    <p><?php the_field('country'); ?></p>
                    <p><?php the_field('address'); ?></p>
                    <p>phone: <?php the_field('phone'); ?><br>email: <?php the_field('email'); ?><br>web: <?php the_field('website'); ?></p>
                  </div>
                </div>
                <?php
              endwhile;
            }
            wp_reset_query();  // Restore global post data stomped by the_post().
          ?>
         
    
    
    
   <?php endwhile; // end of the loop. ?>
 </div>
<?php // get_sidebar(); ?>
<!--<script>
$( "select" )
.change(function () {
var str = "";
$( "select option:selected" ).each(function() {
str += $( this ).text() + " ";
});
$( "div" ).text( str );
})
.change();
</script>-->
<script type="text/javascript">
jQuery(document).ready( function() {
	jQuery( ".retailer_country" ).change(function() {
		
		deleteMarkers();
		var countrycode = $( ".retailer_country option:selected" ).val();
    	var countryname = $( ".retailer_country option:selected" ).text();
		 jQuery.ajax({
         type : "post",
         dataType : "json",
         url : "<?php echo admin_url( 'admin-ajax.php' )?>",
         data : {action: "get_retailer_country_cities", countryId : countrycode, countryName:countryname},
         success: function(response) {
            if(response.type == "success") {
				  jQuery( ".retailer_city" ).find('option').remove().end().append('<option value="">Select City</option>');
               	  //jQuery("#vote_counter").html(response.vote_count)
			      jQuery(".retailer_container").html(response.retailers); 
			  	  jQuery.each(response.data, function(index,value){
				   		console.log(value);
				   		jQuery( ".retailer_city" ).append('<option value="'+index+'">'+value+'</option>');
				   });
				   var marker;
					$.each(response.locationdata, function( index, markerobj ) {
					  console.log(markerobj.lat);
					  
					  marker = new google.maps.Marker({
						/* position: new google.maps.LatLng(locations[i][3], locations[i][4]),*/
						position: new google.maps.LatLng(markerobj.lat, markerobj.lon),
						map: map
					  });
					  markers.push(marker);
					  if (markers.length > 0)
						{
						 var bounds = new google.maps.LatLngBounds();
						 for (var i = 0; i < markers.length; i++) bounds.extend(markers[i].getPosition());
						 map.fitBounds(bounds);
						}
					  //map.setZoom(5);      // This will trigger a zoom_changed on the map
					  //map.setCenter(new google.maps.LatLng(markerobj.lat, markerobj.lon));
				
					});
            }
            else {
               alert("Your vote could not be added")
            }
         }
      });   
	});
	
	
	jQuery( ".retailer_city" ).change(function() {
		
		deleteMarkers();
		var countrycode = $( ".retailer_country option:selected" ).val();
    	var countryname = $( ".retailer_country option:selected" ).text();
		var cityname = $( ".get_city_retailers option:selected" ).text();
		 jQuery.ajax({
         type : "post",
         dataType : "json",
         url : "<?php echo admin_url( 'admin-ajax.php' )?>",
         data : {action: "get_retailer_country_cities", countryId : countrycode, countryName:countryname, cityName:cityname },
         success: function(response) {
            if(response.type == "success") {
			      jQuery(".retailer_container").html(response.retailers); 
				   var marker;
					$.each(response.locationdata, function( index, markerobj ) {
					  console.log(markerobj.lat);
					  
					  marker = new google.maps.Marker({
						/* position: new google.maps.LatLng(locations[i][3], locations[i][4]),*/
						position: new google.maps.LatLng(markerobj.lat, markerobj.lon),
						map: map
					  });
					  markers.push(marker);
					if (markers.length > 0)
						{
						 var bounds = new google.maps.LatLngBounds();
						 for (var i = 0; i < markers.length; i++) bounds.extend(markers[i].getPosition());
						 map.fitBounds(bounds);
						}
					});
            }
            else {
               alert("Your vote could not be added")
            }
         }
      });   
	});
})
</script>

<?php get_footer(); ?>