<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	<footer>
        <div class='row-fluid'>
          <div class='col-md-4 text-left'>
            <div class='copy'>
              &copy; 2014 Barong Barong
            </div>
          </div>
          <div class='col-md-8 navigation'>
            <nav class='pull-right'>
              <ul>
                <li>
                  <a href='#'>Customer Service</a>
                  <ul class='dropdown'>
                    <li>
                      <a href='#'>How to order</a>
                    </li>
                    <li>
                      <a href='#'>Privacy Policy</a>
                    </li>
                    <li>
                      <a href='#'>FAQ</a>
                    </li>
                    <li>
                      <a href='#'>Delivery &amp; returns</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href='#'>Press</a>
                </li>
                <li>
                  <a href='#'>Terms</a>
                </li>
                <li>
                  <a href='#'>Login</a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </footer>
      <!-- jQuery -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js" type="text/javascript"></script>
	  <!-- Image Zoom -->
	  <script src="<?php bloginfo('template_directory'); ?>/javascripts/jquery.elevateZoom-3.0.8.min.js" type="text/javascript"></script>
      <!-- Bootstrap JS -->
      <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>
      <!-- Isotope -->
      <script src="<?php bloginfo('template_directory'); ?>/javascripts/isotope.pkgd.min.js" type="text/javascript"></script>
      <!-- Modernizr -->
      <script src="<?php bloginfo('template_directory'); ?>/javascripts/modernizr.js" type="text/javascript"></script>
      <!-- Functions -->
      <script src="<?php bloginfo('template_directory'); ?>/javascripts/functions.js" type="text/javascript"></script>
    </div>

<?php wp_footer(); ?>
</body>
</html>