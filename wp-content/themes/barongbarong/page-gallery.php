<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0

Template Name: Gallery

 */

get_header(); ?>

	<!-- header foto -->
    <figure class='banner small'>
        <img class="full" src="<?php bloginfo('template_directory'); ?>/images/header-page.jpg" />
    </figure>
    
    <div class='container-fluid'>
    
        <section class='row-fluid text-left' id='first'>
        <div class='col-md-12 text-center'>
          <h1><?php the_title(); ?></h1>
        </div>
        
        
        <div class='col-md-12'>
          <div id='isotope'>
        
            <?php query_posts( array(
     'post_type' => array( 'gallery' ),
     'showposts' => 5 )
     ); ?>
		<?php while ( have_posts() ) : the_post(); ?>
            
            
            
            <div class='item' href='#'>
				<?php
                if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                	the_post_thumbnail();
                } 
                ?>
              <span class='caption'>
                <h4>
                  <?php the_title(); ?>
                </h4>
                <?php the_content() ?>
                <span class='share row'>
                  <div class='col-md-5'>
                    Share online
                  </div>
                  <div class='col-md-7'>
                    <ul class='social-icons'>
                      <!--<li>
                        <a href='#'>
                          <img src="<?php bloginfo('template_directory'); ?>/images/googleplus.png" />
                        </a>
                      </li>-->
                      <li>
                        <a href='#'>
                          <img src="<?php bloginfo('template_directory'); ?>/images/pinterest.png" />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <img src="<?php bloginfo('template_directory'); ?>/images/facebook.png" />
                        </a>
                      </li>
                      <li>
                        <a href='#'>
                          <img src="<?php bloginfo('template_directory'); ?>/images/twitter.png" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </span>
              </span>
            </div>

<?php endwhile; // end of the loop. ?>
</div>
        </div>
      </section>

<?php // get_sidebar(); ?>
<?php get_footer(); ?>