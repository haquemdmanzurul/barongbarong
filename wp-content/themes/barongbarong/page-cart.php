<?php /* Template Name: Custom Woo Cart */ ?>
<?php get_header(); ?>
<?php //print_r(WC()->cart->get_cart()) ?>

   <div class='container-fluid'>
      <section class='top row-fluid text-center' id='first'>
        <div class='col-md-12'>
          <h1>Shopping bag</h1>
        </div>
		<form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">
			<?php do_action( 'woocommerce_before_cart_table' ); ?>
			<div class='col-md-12 cart'>
			  <div class='row titles'>
				<div class='col-md-2'></div>
				<div class='col-md-2'>
				  Description
				</div>
				<div class='col-md-2'>
				  Material
				</div>
				<div class='col-md-2'>
				  Quantity
				</div>
				<div class='col-md-2'>
				  Unit price
				</div>
				<div class='col-md-2'>
				  Total
				</div>
			  </div>
			 
			 <?php do_action( 'woocommerce_before_cart_contents' ); ?>
			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			?>
				  <div class='row product'>
					<div class='col-md-2'>
						<?php
							echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf( '<a href="%s" class="remove" title="%s"> <img src="' . get_bloginfo('template_directory') . '/images/remove.png" /></a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'woocommerce' ) ), $cart_item_key );
							
							$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($product_id), 'full');
							if (!empty($thumbnail)) echo '<a href="' . get_permalink($product_id) . '"><img src="' . $thumbnail[0] . '" style="width: 100px; height: auto;" /></a>';
						?>			  
					</div>
					<div class='col-md-2'>
					  <?php _e($_product->get_title()) ?>
					</div>
					<div class='col-md-2'>
					  <?php
						$term = get_term_by('slug', @$cart_item['variation']['attribute_pa_material'], 'pa_material');
						if ($term) _e($term->name);
					  ?>
					</div>
					<div class='col-md-2'>
						<?php echo $cart_item['quantity']; ?>
					</div>
					<div class='col-md-2'>
						<?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>
					</div>
					<div class='col-md-2'>
						<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
					</div>
				  </div>
				  <?php } ?>
			  <?php } ?>
			  <?php do_action( 'woocommerce_cart_contents' ); ?>
			</div>
			<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
			<?php wp_nonce_field( 'woocommerce-cart' ); ?>
			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
			<?php do_action( 'woocommerce_after_cart_table' ); ?>
		</form>
        <div class='col-md-12 info-total'>
          <div class='row'>
            <div class='col-md-8'>
              Orders shopped within <strong>2 business days</strong>.<br>
              <strong>14-day return</strong> guarantee
            </div>
            <div class='col-md-2 caps text-center'>
              Total
            </div>
            <div class='col-md-2 text-center'>
              &euro; <?php _e(WC()->cart->cart_contents_total) ?>
            </div>
          </div>
        </div>
        <div class='col-md-12 foot'>
          <div class='row text-left'>
            <div class='col-md-9'>
              <a href='<?php _e(get_permalink( woocommerce_get_page_id( 'shop' ) )) ?>'>
                &laquo; Continue shopping
              </a>
            </div>
            <div class='col-md-3'>
			  <?php
				global $woocommerce;
				$checkout_url = $woocommerce->cart->get_checkout_url();
			  ?>			
              <input class='checkout' type='submit' value='Checkout' onclick='window.location.href = "<?php _e($checkout_url) ?>";'>
            </div>
          </div>
        </div>
      </section>
	<?php do_action( 'woocommerce_after_cart' ); ?>
	
<?php get_footer(); ?>