<?php 
	if ( ! defined( 'ABSPATH' ) ) exit;
	global $wp_query;
	
	$args = array(
	  'taxonomy'     => 'product_cat',
	  'hide_empty'   => 0
	);	
	$woo_categories = get_categories( $args );

	$big = 999999999;
	$pagination =  paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages
	) );	
	
	get_header('shop');
?>

<figure class='banner small'>
  <img class="full" src="<?php bloginfo('template_directory'); ?>/images/header-shop.jpg" />
</figure>
<div class='container-fluid'>
  <section class='row-fluid text-left' id='first'>
	<div class='col-md-12 crumbtrail'>
	  <a href="<?php _e(get_bloginfo('url')) ?>">home</a>&nbsp;/&nbsp;<a href="<?php _e(get_permalink( woocommerce_get_page_id( 'shop' ) )) ?>">shop</a>
	  <?php
		global $wp_query;
		if (isset($wp_query->query_vars['product_cat']) && !empty($wp_query->query_vars['product_cat'])) {
			$term = get_term_by('slug', $wp_query->query_vars['product_cat'], 'product_cat');
			$link = get_term_link($wp_query->query_vars['product_cat'], 'product_cat');
			_e('&nbsp;/&nbsp;<a href="' . $link . '">' . $term->name . '</a>');
		}
	  ?>
	</div>
	<div class='col-md-12 text-center'>
	  <h1>Shop</h1>
	</div>
	<div class='col-md-3 sidebar'>
	  <ul>
	    <?php dynamic_sidebar('woocommerce_archive_sidebar') ?>
	  </ul>
	</div>
	<div class='col-md-9'>
	  <div class='row text-center'>
		<?php foreach ($wp_query->posts as $p) { ?>
		  <?php $product = get_product($p->ID); ?>
		  <a class='product col-md-3' href='<?php _e($product->get_permalink()) ?>'>
		    <?php $featured_img = wp_get_attachment_image_src(get_post_thumbnail_id($p->ID), '219x208'); ?>
			<?php if (!empty($featured_img)) { ?>
		      <img src="<?php _e($featured_img[0]) ?>" />
			<?php } ?>
		    <div class='title'>
			  <?php _e($product->get_title()) ?>
		    </div>
		    <div class='price'>
			  <?php _e($product->get_price_html()) ?>
		    </div>
		  </a>
		<?php } ?>
	  </div>
	  <?php _e($pagination) ?>
	</div>
  </section>

<?php get_footer( 'shop' ); ?>