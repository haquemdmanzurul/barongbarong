<?php
	if ( ! defined( 'ABSPATH' ) ) exit;
	global $wp_query, $post, $product;
	
	$product = get_product($post->ID);
	
	$product_images = array();
	$product_images['featured'] = array();
	$product_images['gallery'] = array();
	$featured_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
	if (!empty($featured_img)) {
		$product_images['featured'] = array(
			'thumb' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), '84x80'),
			'med' => wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), '552x426'),
			'large' => $featured_img
		);
	}
	$gallery_attachment_ids = $product->get_gallery_attachment_ids();
	if (!empty($gallery_attachment_ids)) {
		foreach ($gallery_attachment_ids as $att_id) {
			$product_images['gallery'][] = array(
				'thumb' => wp_get_attachment_image_src($att_id, '84x80'),
				'med' => wp_get_attachment_image_src($att_id, '552x426'),
				'large' => wp_get_attachment_image_src($att_id, 'full')
			);
		}
	}
	
	$post_content = apply_filters('the_content', $post->post_content);	
	
	$details_and_care = get_post_meta($post->ID, '_details_and_care', true);
	$details_and_care = stripslashes($details_and_care);
	$details_and_care = apply_filters('the_content', $details_and_care);
	
	$delivery_and_returns = get_post_meta($post->ID, '_delivery_and_returns', true);
	$delivery_and_returns = stripslashes($delivery_and_returns);
	$delivery_and_returns = apply_filters('the_content', $delivery_and_returns);
	
	$related_products = $product->get_related(6);
	
	$default_attributes = get_post_meta($post->ID, '_default_attributes');
	if (!empty($default_attributes)) $default_attributes = @$default_attributes[0];
	
	get_header('shop');
?>

<script type="text/javascript">
	IS_PRODUCT_IN_STOCK = <?php _e($product->product_type === 'variable' ? 'false' : $product->is_in_stock() ? 'true' : 'false') ?>;
	IS_SINGLE_PRODUCT_AJAX_WORKING = false;	
</script>

<figure class='banner small'>
  <img class="full" src="<?php bloginfo('template_directory'); ?>/images/header-shop.jpg" />
</figure>
<div class='container-fluid'>
  <section class='row-fluid text-left' id='first'>
	<div class='col-md-12 crumbtrail back'>
	  Back to <?php _e($product->get_categories()) ?>
	</div>
	<div class='col-md-6'>
	  <?php if (!empty($product_images)) { ?>
	    <?php if (!empty($product_images['featured'])) { ?>
	      <div class='product-large'>
  		    <img src="<?php _e($product_images['featured']['med'][0]) ?>" data-zoom-image="<?php _e($product_images['featured']['large'][0]) ?>" data-thumb="<?php _e($product_images['featured']['thumb'][0]) ?>" />
	      </div>
		<?php } ?>
		<?php if (!empty($product_images['gallery'])) { ?>
	      <div class='products-small row'>
			<?php foreach ($product_images['gallery'] as $gal) { ?>
		      <a href='#' class='col-md-2'>
		        <img src="<?php _e($gal['thumb'][0]) ?>" data-med="<?php _e($gal['med'][0]) ?>" data-large="<?php _e($gal['large'][0]) ?>" />
		      </a>
			<?php } ?>
	      </div>
	    <?php } ?>
	  <?php } ?>
	</div>
	<div class='col-md-6'>
	  <h2>
		<?php _e($post->post_title) ?>
	  </h2>
	  <div class='pricing row'>
		<div class='price col-md-4'>
		  <?php _e($product->product_type === 'variable' ? '' : $product->is_in_stock() ? $product->get_price_html() : out_of_stock_html()) ?>
		</div>
		<div class='social-icons wide col-md-8 text-right'>
		  <a class='icon' href='#'>
			<img src="<?php bloginfo('template_directory'); ?>/images/icon-facebook-square.png" />
		  </a>
		  <a class='icon' href='#'>
			<img src="<?php bloginfo('template_directory'); ?>/images/icon-pinterest-square.png" />
		  </a>
		  <a class='icon' href='#'>
			<img src="<?php bloginfo('template_directory'); ?>/images/icon-googleplus-square.png" />
		  </a>
		  <a class='icon' href='#'>
			<img src="<?php bloginfo('template_directory'); ?>/images/icon-twitter-square.png" />
		  </a>
		</div>
	  </div>
	  <div class='panel-group' id='accordion'>
		<?php if (!empty($post_content)) { ?>
			<div class='panel panel-default'>
			  <div class='panel-heading'>
				<h4 class='panel-title'>
				  <a class='toggle collapsed' data-parent='#accordion' data-toggle='collapse' href='#one'>
					Notes
				  </a>
				</h4>
			  </div>
			  <div class='panel-collapse collapse in' id='one'>
				<div class='panel-body'>
				  <?php _e($post_content) ?>
				</div>
			  </div>
			</div>
		<?php } ?>
		<?php if (!empty($details_and_care)) { ?>
		<div class='panel panel-default'>
		  <div class='panel-heading'>
			<h4 class='panel-title'>
			  <a class='toggle collapsed' data-parent='#accordion' data-toggle='collapse' href='#two'>
				Details &amp; care
			  </a>
			</h4>
		  </div>
		  <div class='panel-collapse collapse' id='two'>
			<div class='panel-body'>
			  <?php _e($details_and_care) ?>
			</div>
		  </div>
		</div>
		<?php } ?>
		<?php if (!empty($delivery_and_returns)) { ?>
		<div class='panel panel-default'>
		  <div class='panel-heading'>
			<h4 class='panel-title'>
			  <a class='toggle collapsed' data-parent='#accordion' data-toggle='collapse' href='#three'>
				Delivery &amp; returns
			  </a>
			</h4>
		  </div>
		  <div class='panel-collapse collapse' id='three'>
			<div class='panel-body'>
			  <?php _e($delivery_and_returns) ?>
			</div>
		  </div>
		</div>
		<?php } ?>
	  </div>
	  <?php foreach ($product->get_attributes() as $attr) { ?>
		  <?php if (!$attr['is_visible']) continue; ?>
		  <div class='options product-attributes'>
			<div class='title'><?php _e(str_replace('pa_', '', $attr['name'])) ?>:</div>
			<?php foreach (wc_get_product_terms( $post->ID, $attr['name'], array( 'fields' => 'names' ) ) as $val) { ?>
				<?php $val = trim($val); ?>
				<a class='label <?php _e($default_attributes[strtolower($attr['name'])] == str_replace(' ', '-', $val) ? 'active' : '') ?>' href='#' data-key='attribute_<?php _e(strtolower($attr['name'])) ?>' data-val='<?php _e(strtolower($val)) ?>'><?php _e(ucfirst($val)) ?></a>
			<?php } ?>
		  </div>	  
	  <?php } ?>
	  <form class='text-right pull-right buy-form' action="<?php _e(get_cart_page_url()) ?>" method="POST">
	    <input type='hidden' name='add-to-cart' value='<?php _e($post->ID) ?>'>
		<input type='hidden' name='variation_id' value=''>
		<input type='submit' value='Add to shoppingbag' class='add-to-bag'>
		<label>
		  Questions?
		  <a href='#'>Contact us</a>
		</label>
		<div style="display: none !important;" class="attrs"></div>
	  </form>
	</div>
	<?php if (!empty($related_products)) { ?>
	<div class='col-md-12 separator'>
	  <h4 class='thin'>You may also like</h4>
	  <div class='row text-center'>
		<?php foreach ($related_products as $related_product_id) { ?>
		  <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id($related_product_id), 'full'); ?>
		    <?php if ($img) { ?>
		      <a href="<?php _e(get_permalink($related_product_id)) ?>" class='col-md-2'>
		        <img src="<?php _e($img[0]) ?>" />
		      </a>
		    <?php } ?>
		<?php } ?>
	  </div>
	</div>
	<?php } ?>
  </section>
  
<?php get_footer( 'shop' ); ?>