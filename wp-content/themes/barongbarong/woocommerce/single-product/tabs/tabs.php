<?php
/**
 * Single Product tabs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>
	<div class='col-md-6'>
        <div class='panel-group' id='accordion'>
            
                <?php 
				$i = 0;
				foreach ( $tabs as $key => $tab ) : 
				$i++;
				?>
                     <div class='panel panel-default'>
                  <div class='panel-heading'>
                    <h4 class='panel-title'>
                    <a class='toggle collapsed' data-parent='#accordion' data-toggle='collapse' href='#<?php echo $key ?>_tab'>
                        <?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', $tab['title'], $key ) ?>
                      </a>
                    </h4>
                    </div>
                  <div class='panel-collapse collapse <?php if ($i == '1') { echo "in"; } ?>' id='<?php echo $key ?>_tab'>
                    <div class='panel-body'>
                    <?php call_user_func( $tab['callback'], $key, $tab ) ?>
                    </div>
                  </div>
                </div>
                <?php endforeach; ?>
            
        </div>
    </div>

<?php endif; ?>